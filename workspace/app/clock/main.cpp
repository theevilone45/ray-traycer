#include <filesystem>
#include <iostream>

#include "common/Constants.hpp"
#include "files/PPMFile.hpp"
#include "transformations/Transformations.hpp"
#include "types/Point.hpp"

namespace {
const std::filesystem::path cRendersDir = "/home/marcins/ray-traycer/renders";
}   // namespace

class Clock {
   public:
    Clock()
        : mOrigin(rt::makePoint(0, 0, 0)), mCanvas(rt::makeCanvas(500, 500)) {}

    void generatePoints() {
        mPoints.at(0) = rt::translation(250.f, 250.f, 0.0f) * mOrigin;

        const auto translation = rt::translation(0.0f, 1.0f, 0.0f);
        mOrigin = translation * mOrigin;

        for (uint32_t i = 1; i < 13; i++) {
            const auto transform = rt::Transform4f()
                                       .zRotate(i * rt::cPi / 6.0)
                                       .scale(3.0f / 8.0f * mCanvas.width,
                                              3.0f / 8.0f * mCanvas.width, 0.0f)
                                       .translate(250.0f, 250.0f, 0.0f)
                                       .get();

            mPoints.at(i) = transform * mOrigin;
            std::cout << mPoints.at(i) << '\n';
        }
    }

    void save() {
        for (const auto point : mPoints) {
            const auto x = static_cast<uint32_t>(point.x);
            const auto y = mCanvas.height - static_cast<uint32_t>(point.y) - 1;
            // std::cout << point << '\n';
            mCanvas(x, y) = rt::color::White;
        }
        rt::PPM::save(mCanvas, cRendersDir / "Clock.ppm");
    }

   private:
    rt::Point mOrigin;
    std::array<rt::Point, 13> mPoints;
    rt::Canvas mCanvas;
};

int main() {
    Clock clock;
    clock.generatePoints();
    clock.save();
}