#include <filesystem>
#include <iostream>
#include <memory>

#include "camera/Camera.hpp"
#include "common/Constants.hpp"
#include "files/PPMFile.hpp"
#include "objects/Material.hpp"
#include "objects/Plane.hpp"
#include "objects/Sphere.hpp"
#include "transformations/Transformations.hpp"
#include "transformations/View.hpp"
#include "types/Canvas.hpp"
#include "world/World.hpp"

namespace {
const std::filesystem::path cRendersDir = std::filesystem::current_path();
constexpr const uint32_t cImageWidth = 800;
constexpr const uint32_t cImageHeight = 600;

[[maybe_unused]] auto makeFloor() {
    return rt::PlaneBuilder()
        .material(rt::MaterialBuilder().reflective(0.2f).get())
        .get();
}
[[maybe_unused]] auto makeLeftWall() {
    return rt::PlaneBuilder()
        .transformation(rt::Transform4f()
                            .yRotate(-rt::cPi / 2.0f)
                            .translate(0.0f, 0.0f, 1.0f)
                            .get())
        .material(rt::MaterialBuilder()
                      .color(rt::makeColor(0.5f, 0.5f, 0.6f))
                      .specular(0.1f)
                      .get())
        .get();
}
[[maybe_unused]] auto makeRightWall() {
    return rt::SphereBuilder()
        .transformation(rt::Transform4f()
                            .scale(10.0f, 0.05f, 10.0f)
                            .xRotate(rt::cPi / 2.0f)
                            .yRotate(rt::cPi / 4.0f)
                            .translate(0.0f, 0.0f, 5.0f)
                            .get())
        .material(rt::MaterialBuilder()
                      .color(rt::makeColor(0.5f, 0.5f, 0.6f))
                      .specular(0.1f)
                      .get())
        .get();
}
[[maybe_unused]] auto makeMiddleSphere() {
    return rt::SphereBuilder()
        .transformation(rt::translation(-1.f, 1.0f, 0.0f))
        .material(rt::MaterialBuilder()
                      .color(rt::makeColor(1.0f, 0.0f, 0.5f))
                      .specular(0.7f)
                      .diffuse(0.7f)
                      .reflective(0.1f)
                      .get())
        .get();
}
[[maybe_unused]] auto makeRightSphere() {
    return rt::SphereBuilder()
        .transformation(rt::Transform4f()
                            .scale(0.7f, 0.7f, 0.7f)
                            .translate(1.0f, 0.7f, 0.7f)
                            .get())
        .material(rt::MaterialBuilder()
                      .reflective(1.0f)
                      .color(rt::color::Black)
                      .get())
        .get();
}

}   // namespace

class Scene {
   public:
    using PathType = std::filesystem::path;

   public:
    /// @brief adds objects to scene
    void setUpWorld();

    /// @brief renders image on canvas
    void render();

   private:
    /// @brief saves canvas into ppm file
    /// @param path output file path
    void saveToFile(const rt::Canvas& canvas, const PathType& path) const;

   private:
    rt::World mWorld;
    rt::Camera mCamera;

   private:
    enum class SceneStep {
        INITIAL = 0,
        WORLD_SET_UP,
        RENDERED
    };

    SceneStep mStep = SceneStep::INITIAL;
};

int main() {
    Scene scene;
    scene.setUpWorld();
    scene.render();
    return 0;
}

void Scene::setUpWorld() {
    assert(mStep == SceneStep::INITIAL);

    const auto light = rt::makePointLight(rt::color::White,
                                          rt::makePoint(-10.0f, 10.0f, -10.0f));
    mWorld = rt::WorldBuilder()
                 .addLight(light)
                 .addObject(makeMiddleSphere())
                 .addObject(makeFloor())
                 .addObject(makeRightSphere())
                 .get();
    mCamera = rt::CameraBuilder()
                  .dimentions(cImageWidth, cImageHeight)
                  .fov(rt::cPi / 3.0f)
                  .transform(rt::makeView(rt::makePoint(0.0f, 1.5f, -5.0f),
                                          rt::makePoint(0.0f, 1.0f, 0.0f),
                                          rt::makeVector(0.0f, 1.0f, 0.0f)))
                  .get();

    mStep = SceneStep::WORLD_SET_UP;
}

void Scene::render() {
    assert(mStep == SceneStep::WORLD_SET_UP);
    const auto image = mCamera.render(mWorld);
    saveToFile(image, cRendersDir / "scene1.ppm");
    std::cout << "File saved in: " << cRendersDir / "scene1.ppm\n";
}

void Scene::saveToFile(const rt::Canvas& canvas, const PathType& path) const {
    rt::PPM::save(canvas, path);
}