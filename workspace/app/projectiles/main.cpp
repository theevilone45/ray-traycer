#include <filesystem>
#include <iostream>

#include "files/PPMFile.hpp"
#include "types/Point.hpp"
#include "types/Vector.hpp"

namespace {
const std::filesystem::path cRendersDir = "/home/marcins/ray-traycer/renders";
}

struct Projectile {
    rt::Point position;
    rt::Vector velocity;
};

struct Environment {
    rt::Vector gravity;
    rt::Vector wind;
};

Projectile tick(const Projectile& projectile, const Environment& environment) {
    rt::Point position = projectile.position + projectile.velocity;
    rt::Vector velocity =
        projectile.velocity + environment.gravity + environment.wind;
    return {position, velocity};
}

int main() {
    auto projectile = Projectile{
        rt::makePoint(0, 1, 0), rt::makeVector(1, 1.8, 0).normalized() * 11.25};
    auto environment =
        Environment{rt::makeVector(0, -0.1, 0), rt::makeVector(-0.01, 0, 0)};

    auto canvas = rt::makeCanvas(900, 550);
    const auto color = rt::color::Red;

    while (true) {
        projectile = tick(projectile, environment);
        const auto x = static_cast<uint32_t>(projectile.position.x);
        const auto y =
            canvas.height - static_cast<uint32_t>(projectile.position.y) - 1;
        std::cout << "x: " << x << " y: " << y << "\n";
        std::cout << "float x: " << projectile.position.x
                  << " float y: " << projectile.position.y << "\n";

        if (projectile.position.y <= 0) {
            break;
        }

        canvas(x, y) = color;
    }

    rt::PPM::save(canvas, cRendersDir / "Projectiles.ppm");

    return 0;
}