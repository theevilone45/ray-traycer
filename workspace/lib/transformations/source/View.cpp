#include "transformations/View.hpp"

namespace rt {
View makeView(const Point& from, const Point& to, const Vector& up) {
    const auto forwardVec = (to - from).normalized();
    const auto upNormalized = up.normalized();
    const auto leftVec = forwardVec.cross(upNormalized);
    const auto trueUpVec = leftVec.cross(forwardVec);
    const Matrix4f orientation =
        // clang-format off
    {
        { leftVec.x,     leftVec.y,     leftVec.z,     0.0f },
        { trueUpVec.x,   trueUpVec.y,   trueUpVec.z,   0.0f },
        { -forwardVec.x, -forwardVec.y, -forwardVec.z, 0.0f },
        { 0.0f,          0.0f,          0.0f,          1.0f }
    };
    //clang-format on

    return orientation * translation(-from.x, -from.y, -from.z);
}
}   // namespace rt