/*
##############################
# LibGen 1.0                 #
# Author: theevilone45       #
# Created: 20.04.2023        #
##############################
 */

#include "transformations/Transformations.hpp"

namespace rt {
Transform4f::Transform4f() : mTransform(Matrix4f::identity()) {}

Transform4f& Transform4f::xRotate(float radians) {
    mTransform = xRotation(radians) * mTransform;
    return *this;
}
Transform4f& Transform4f::yRotate(float radians) {
    mTransform = yRotation(radians) * mTransform;
    return *this;
}
Transform4f& Transform4f::zRotate(float radians) {
    mTransform = zRotation(radians) * mTransform;
    return *this;
}

Transform4f& Transform4f::translate(float x, float y, float z) {
    mTransform = translation(x, y, z) * mTransform;
    return *this;
}

Transform4f& Transform4f::shear(float xy, float xz, float yx, float yz,
                                float zx, float zy) {
    mTransform = skew(xy, xz, yx, yz, zx, zy) * mTransform;
    return *this;
}

Transform4f& Transform4f::scale(float x, float y, float z) {
    mTransform = scaling(x, y, z) * mTransform;
    return *this;
}

Transform4f& Transform4f::transform(const Matrix4f& t) {
    mTransform = t * mTransform;
    return *this;
}

const Matrix4f& Transform4f::get() const { return mTransform; }
}   // namespace rt
