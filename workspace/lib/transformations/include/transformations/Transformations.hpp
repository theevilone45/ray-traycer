#ifndef WORKSPACE_LIB_TRANSFORMATIONS_INCLUDE_TRANSFORMATIONS_TRANSFORMATIONS
#define WORKSPACE_LIB_TRANSFORMATIONS_INCLUDE_TRANSFORMATIONS_TRANSFORMATIONS

#include "types/Matrix.hpp"
#include "types/Tuple.hpp"

namespace rt {
template <typename ValueType>
Matrix<4, ValueType> translation(const ValueType& x, const ValueType& y,
                                 const ValueType& z) {
    auto result = Matrix<4, ValueType>::identity();
    result(0, 3) = x;
    result(1, 3) = y;
    result(2, 3) = z;
    return result;
}

template <typename ValueType>
Matrix<4, ValueType> scaling(const ValueType& x, const ValueType& y,
                             const ValueType& z) {
    auto result = Matrix<4, ValueType>::identity();
    result(0, 0) = x;
    result(1, 1) = y;
    result(2, 2) = z;
    return result;
}

template <typename ValueType>
Matrix<4, ValueType> xRotation(const ValueType& radians) {
    auto result = Matrix<4, ValueType>::identity();
    const auto cosR = std::cos(radians);
    const auto sinR = std::sin(radians);
    result(1, 1) = cosR;
    result(1, 2) = -sinR;
    result(2, 1) = sinR;
    result(2, 2) = cosR;
    return result;
}

template <typename ValueType>
Matrix<4, ValueType> yRotation(const ValueType& radians) {
    auto result = Matrix<4, ValueType>::identity();
    const auto cosR = std::cos(radians);
    const auto sinR = std::sin(radians);
    result(0, 0) = cosR;
    result(0, 2) = sinR;
    result(2, 0) = -sinR;
    result(2, 2) = cosR;
    return result;
}

template <typename ValueType>
Matrix<4, ValueType> zRotation(const ValueType& radians) {
    auto result = Matrix<4, ValueType>::identity();
    const auto cosR = std::cos(radians);
    const auto sinR = std::sin(radians);
    result(0, 0) = cosR;
    result(0, 1) = -sinR;
    result(1, 0) = sinR;
    result(1, 1) = cosR;
    return result;
}

template <typename ValueType>
Matrix<4, ValueType> skew(const ValueType& xy, const ValueType& xz,
                          const ValueType& yx, const ValueType& yz,
                          const ValueType& zx, const ValueType& zy) {
    auto result = Matrix<4, ValueType>::identity();
    result(0, 1) = xy;
    result(0, 2) = xz;
    result(1, 0) = yx;
    result(1, 2) = yz;
    result(2, 0) = zx;
    result(2, 1) = zy;
    return result;
}

class Transform4f {
   public:
    Transform4f();

    Transform4f& xRotate(float radians);
    Transform4f& yRotate(float radians);
    Transform4f& zRotate(float radians);

    Transform4f& translate(float x, float y, float z);

    // skew
    Transform4f& shear(float xy, float xz, float yx, float yz, float zx,
                       float zy);

    Transform4f& scale(float x, float y, float z);

    Transform4f& transform(const Matrix4f& t);

    const Matrix4f& get() const;

   private:
    Matrix4f mTransform;
};

}   // namespace rt

#endif /* WORKSPACE_LIB_TRANSFORMATIONS_INCLUDE_TRANSFORMATIONS_TRANSFORMATIONS \
        */
