#ifndef WORKSPACE_LIB_TRANSFORMATIONS_INCLUDE_TRANSFORMATIONS_VIEW
#define WORKSPACE_LIB_TRANSFORMATIONS_INCLUDE_TRANSFORMATIONS_VIEW

#include "transformations/Transformations.hpp"
#include "types/Matrix.hpp"
#include "types/Point.hpp"
#include "types/Vector.hpp"

namespace rt {
using View = rt::Matrix4f;

View makeView(const Point& from, const Point& to, const Vector& up);
}   // namespace rt

#endif /* WORKSPACE_LIB_TRANSFORMATIONS_INCLUDE_TRANSFORMATIONS_VIEW */
