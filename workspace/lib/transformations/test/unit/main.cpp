/*
##############################
# LibGen 1.0                 #
# Author: theevilone45       #
# Created: 20.04.2023        #
##############################
 */
#include <gtest/gtest.h>

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
