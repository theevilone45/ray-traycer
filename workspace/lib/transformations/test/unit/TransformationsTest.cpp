#include <gtest/gtest.h>

#include "common/Constants.hpp"
#include "transformations/Transformations.hpp"
#include "types/Point.hpp"
#include "types/Vector.hpp"

TEST(TranslationTest, TranslationMatrix) {
    const auto transform = rt::translation(5.0f, -3.0f, 2.0f);
    const auto point = rt::makePoint(-3.0f, 4.0f, 5.0f);
    ASSERT_EQ(transform * point, rt::makePoint(2.0f, 1.0f, 7.0f));
}

TEST(TranslationTest, InverseTranslationMatrix) {
    const auto transform = rt::translation(5.0f, -3.0f, 2.0f);
    const auto inv = transform.inverted();
    const auto point = rt::makePoint(-3.0f, 4.0f, 5.0f);
    ASSERT_EQ(inv * point, rt::makePoint(-8.0f, 7.0f, 3.0f));
}

TEST(TranslationTest, TranlatingVector) {
    const auto transform = rt::translation(5.0f, -3.0f, 2.0f);
    const auto vec = rt::makeVector(-3.0, 4.0, 5.0);
    ASSERT_EQ(transform * vec, vec);
}

TEST(ScalingTest, ScalingPoint) {
    const auto transform = rt::scaling(2.0f, 3.0f, 4.0f);
    const auto point = rt::makePoint(-4.0f, 6.0f, 8.0f);
    ASSERT_EQ(transform * point, rt::makePoint(-8.0f, 18.0f, 32.0f));
}

TEST(ScalingTest, ScalingVector) {
    const auto transform = rt::scaling(2.0f, 3.0f, 4.0f);
    const auto vec = rt::makeVector(-4.0f, 6.0f, 8.0f);
    ASSERT_EQ(transform * vec, rt::makeVector(-8.0f, 18.0f, 32.0f));
}

TEST(ScalingTest, Reflection) {
    const auto transform = rt::scaling(-1.0f, 1.0f, 1.0f);
    const auto point = rt::makePoint(2.0f, 3.0f, 4.0f);
    const auto vec = rt::makeVector(2.0f, 3.0f, 4.0f);
    ASSERT_EQ(transform * point, rt::makePoint(-2.0f, 3.0f, 4.0f));
    ASSERT_EQ(transform * vec, rt::makeVector(-2.0f, 3.0f, 4.0f));
}

TEST(RotationTest, XAxisRotation) {
    const auto point = rt::makePoint(0.0f, 1.0f, 0.0f);
    const auto halfQuarter = rt::xRotation(rt::cPi / 4.0f);
    const auto fullQuarter = rt::xRotation(rt::cPi / 2.0f);
    ASSERT_EQ(halfQuarter * point,
              rt::makePoint(0.0f, rt::cSqrt2 / 2.0f, rt::cSqrt2 / 2.0f));
    ASSERT_EQ(fullQuarter * point, rt::makePoint(0.0f, 0.0f, 1.0f));
}

TEST(RotationTest, InvertXAxisRotation) {
    const auto point = rt::makePoint(0.0f, 1.0f, 0.0f);
    const auto halfQuarter = rt::xRotation(rt::cPi / 4.0f);
    const auto invertedRotation = halfQuarter.inverted();
    ASSERT_EQ(invertedRotation * point,
              rt::makePoint(0.0f, rt::cSqrt2 / 2.0f, -rt::cSqrt2 / 2.0f));
}

TEST(RotationTest, YAxisRotation) {
    const auto point = rt::makePoint(0.0f, 0.0f, 1.0f);
    const auto halfQuarter = rt::yRotation(rt::cPi / 4.0f);
    const auto fullQuarter = rt::yRotation(rt::cPi / 2.0f);
    ASSERT_EQ(halfQuarter * point,
              rt::makePoint(rt::cSqrt2 / 2.0f, 0.0f, rt::cSqrt2 / 2.0f));
    ASSERT_EQ(fullQuarter * point, rt::makePoint(1.0f, 0.0f, 0.0f));
}

TEST(RotationTest, ZAxisRotation) {
    const auto point = rt::makePoint(0.0f, 1.0f, 0.0f);
    const auto halfQuarter = rt::zRotation(rt::cPi / 4.0f);
    const auto fullQuarter = rt::zRotation(rt::cPi / 2.0f);
    ASSERT_EQ(halfQuarter * point,
              rt::makePoint(-rt::cSqrt2 / 2.0f, rt::cSqrt2 / 2.0f, 0.0f));
    ASSERT_EQ(fullQuarter * point, rt::makePoint(-1.0f, 0.0f, 0.0f));
}

TEST(SkewTest, XYSkew) {
    const auto transform = rt::skew(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
    const auto point = rt::makePoint(2.0f, 3.0f, 4.0f);
    ASSERT_EQ(transform * point, rt::makePoint(5.0f, 3.0f, 4.0f));
}

TEST(SkewTest, XZSkew) {
    const auto transform = rt::skew(0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f);
    const auto point = rt::makePoint(2.0f, 3.0f, 4.0f);
    ASSERT_EQ(transform * point, rt::makePoint(6.0f, 3.0f, 4.0f));
}

TEST(SkewTest, YXSkew) {
    const auto transform = rt::skew(0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f);
    const auto point = rt::makePoint(2.0f, 3.0f, 4.0f);
    ASSERT_EQ(transform * point, rt::makePoint(2.0f, 5.0f, 4.0f));
}

TEST(SkewTest, YZSkew) {
    const auto transform = rt::skew(0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);
    const auto point = rt::makePoint(2.0f, 3.0f, 4.0f);
    ASSERT_EQ(transform * point, rt::makePoint(2.0f, 7.0f, 4.0f));
}

TEST(SkewTest, ZXSkew) {
    const auto transform = rt::skew(0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);
    const auto point = rt::makePoint(2.0f, 3.0f, 4.0f);
    ASSERT_EQ(transform * point, rt::makePoint(2.0f, 3.0f, 6.0f));
}

TEST(SkewTest, ZYSkew) {
    const auto transform = rt::skew(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);
    const auto point = rt::makePoint(2.0f, 3.0f, 4.0f);
    ASSERT_EQ(transform * point, rt::makePoint(2.0f, 3.0f, 7.0f));
}

class ChainedTransformTest : public ::testing::Test {
   protected:
    ChainedTransformTest() {}
    ~ChainedTransformTest() {}

   protected:
    const rt::Point mPoint = rt::makePoint(1.0f, 0.0f, 1.0f);
    const rt::Point mRotationResult = rt::makePoint(1.0f, -1.0f, 0.0f);
    const rt::Point mScalingResult = rt::makePoint(5.0f, -5.0f, 0.0f);
    const rt::Point mTranslationResult = rt::makePoint(15.0f, 0.0f, 7.0f);

    const rt::Matrix4f mRotatation = rt::xRotation(rt::cPi / 2.0f);
    const rt::Matrix4f mScaling = rt::scaling(5.0f, 5.0f, 5.0f);
    const rt::Matrix4f mTranslation = rt::translation(10.0f, 5.0f, 7.0f);
};

TEST_F(ChainedTransformTest, IndividualTransformsInSequence) {
    const auto point2 = mRotatation * mPoint;
    ASSERT_EQ(point2, mRotationResult);
    const auto point3 = mScaling * point2;
    ASSERT_EQ(point3, mScalingResult);
    const auto point4 = mTranslation * point3;
    ASSERT_EQ(point4, mTranslationResult);
}

TEST_F(ChainedTransformTest, TransformsChainedInSequence) {
    const auto transform = rt::Transform4f()
                               .xRotate(rt::cPi / 2.0f)
                               .scale(5.0f, 5.0f, 0.0f)
                               .translate(10.0f, 5.0f, 7.0f)
                               .get();
    const auto transform2 = rt::Transform4f()
                                .transform(mRotatation)
                                .transform(mScaling)
                                .transform(mTranslation)
                                .get();
    ASSERT_EQ(transform * mPoint, mTranslationResult);
    ASSERT_EQ(transform2 * mPoint, mTranslationResult);
}