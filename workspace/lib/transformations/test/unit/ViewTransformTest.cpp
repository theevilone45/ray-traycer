#include <gtest/gtest.h>

#include <iostream>
#include <vector>

#include "transformations/Transformations.hpp"
#include "transformations/View.hpp"
#include "types/Matrix.hpp"
#include "types/Point.hpp"
#include "types/Vector.hpp"

struct ViewTransformParams {
    rt::Point from;
    rt::Point to;
    rt::Vector up;
    rt::View result;
};

class ViewTransformTest : public testing::TestWithParam<ViewTransformParams> {};

TEST_P(ViewTransformTest, ViewTransformTestCase) {
    const auto t = rt::makeView(GetParam().from, GetParam().to, GetParam().up);
    const auto result = GetParam().result;

    std::cout << "t: \n" << t << '\n';
    std::cout << "result: \n" << result << '\n';

    ASSERT_EQ(t, GetParam().result);
}

// clang-format off
std::vector<ViewTransformParams> testParams {
    {
        rt::makePoint(0.0f,0.0f,0.0f),
        rt::makePoint(0.0f,0.0f,-1.0f),
        rt::makeVector(0.0f,1.0f,0.0f),
        rt::cIdentity4f
    },
    {
        rt::makePoint(0.0f,0.0f,0.0f),
        rt::makePoint(0.0f,0.0f,1.0f),
        rt::makeVector(0.0f,1.0f,0.0f),
        rt::scaling(-1.0f,1.0f,-1.0f)
    },
    {
        rt::makePoint(0.0f,0.0f,8.0f),
        rt::makePoint(0.0f,0.0f,0.0f),
        rt::makeVector(0.0f,1.0f,0.0f),
        rt::translation(0.0f,0.0f,-8.0f)
    },
    {
        rt::makePoint(1.0f,3.0f,2.0f),
        rt::makePoint(4.0f,-2.0f,8.0f),
        rt::makeVector(1.0f,1.0f,0.0f),
        {
            { -0.50709f , 0.50709f , 0.67612f  , -2.36643f },
            { 0.76772f  , 0.60609f , 0.12122f  , -2.82843f },
            { -0.35857f , 0.59761f , -0.71714f , 0.00000f  },
            { 0.0f      , 0.0f     , 0.0f      , 1.00000f  }
        }
        
    }
};
//clang-format on

INSTANTIATE_TEST_SUITE_P(TestingViewTransform, ViewTransformTest, testing::ValuesIn(testParams));