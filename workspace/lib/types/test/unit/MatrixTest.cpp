#include <gtest/gtest.h>

#include <types/Matrix.hpp>
#include <types/Vector.hpp>

class MatrixTest : public ::testing::Test {
   protected:
    // clang-format off
    MatrixTest()
        : mMatrix4f({
                {1, 2, 3, 4}, 
                {5, 6, 7, 8}, 
                {9, 8, 7, 6}, 
                {5, 4, 3, 2}
          }),
          mMatrix4fB({
                {-2, 1, 2, 3}, 
                {3, 2, 1, -1}, 
                {4, 3, 6, 5}, 
                {1, 2, 7, 8}
          }),
          mMatrix3f({
                {-3, 5, 0}, 
                {1, -2, -7}, 
                {0, 1, 1}
          }),
          mMatrix2f({
                {-3, 5}, 
                {1, -2}
          }),
          mInvertibleMatrix({
                {6, 4, 4, 4},
                {5, 5, 7, 6},
                {4, -9, 3, -7},
                {9, 1, 7, -6}
          }),
          mNonInvertibleMatrix({
                {-4, 2, -2 ,-3},
                {9, 6, 2, 6},
                {0, -5, 1, -5},
                {0, 0, 0, 0}
          }),
          mTuple({1, 2, 3, 1}) {}
    // clang-format on
    ~MatrixTest() {}

   protected:
    const rt::Matrix4f mMatrix4f;
    const rt::Matrix4f mMatrix4fB;
    const rt::Matrix3f mMatrix3f;
    const rt::Matrix2f mMatrix2f;
    const rt::Matrix4f mInvertibleMatrix;
    const rt::Matrix4f mNonInvertibleMatrix;
    const rt::Tuple mTuple;
};

TEST_F(MatrixTest, CheckMatrix4fValues) {
    ASSERT_FLOAT_EQ(mMatrix4f(0, 0), 1.0);
    ASSERT_FLOAT_EQ(mMatrix4f(0, 3), 4.0);
    ASSERT_FLOAT_EQ(mMatrix4f(1, 2), 7.0);
    ASSERT_FLOAT_EQ(mMatrix4f(3, 3), 2.0);
}

TEST_F(MatrixTest, CheckMatrix3fValues) {
    ASSERT_FLOAT_EQ(mMatrix3f(0, 0), -3);
    ASSERT_FLOAT_EQ(mMatrix3f(1, 1), -2);
    ASSERT_FLOAT_EQ(mMatrix3f(2, 1), 1);
    ASSERT_FLOAT_EQ(mMatrix3f(2, 2), 1);
}

TEST_F(MatrixTest, CheckMatrix2fValues) {
    ASSERT_FLOAT_EQ(mMatrix2f(0, 0), -3);
    ASSERT_FLOAT_EQ(mMatrix2f(0, 1), 5);
    ASSERT_FLOAT_EQ(mMatrix2f(1, 0), 1);
    ASSERT_FLOAT_EQ(mMatrix2f(1, 1), -2);
}

TEST_F(MatrixTest, EqualityOperator) {
    const rt::Matrix4f equal = {
        {1, 2, 3, 4}, {5, 6, 7, 8}, {9, 8, 7, 6}, {5, 4, 3, 2}};
    const rt::Matrix4f notEqual = {
        {5, 6, 7, 8}, {9, 8, 7, 6}, {5, 4, 3, 2}, {1, 2, 3, 4}};
    ASSERT_EQ(equal, mMatrix4f);
    ASSERT_NE(notEqual, mMatrix4f);
}

TEST_F(MatrixTest, MatrixMultiplication) {
    const rt::Matrix4f result4f = {{20, 22, 50, 48},
                                   {44, 54, 114, 108},
                                   {40, 58, 110, 102},
                                   {16, 26, 46, 42}};
    ASSERT_EQ(result4f, mMatrix4f * mMatrix4fB);

    const rt::Matrix3f result3f = {{14, -25, -35}, {-5, 2, 7}, {1, -1, -6}};
    ASSERT_EQ(result3f, mMatrix3f * mMatrix3f);
}

TEST_F(MatrixTest, MatrixTupleMultiplication) {
    const rt::Tuple result = {18, 46, 52, 24};
    ASSERT_EQ(result, mMatrix4f * mTuple);
}

TEST_F(MatrixTest, Matrix3fVectorMultiplication) {
    const rt::Vector result = rt::makeVector(7, -24, 5);
    const rt::Vector vec = rt::makeVector(1, 2, 3);
    ASSERT_EQ(result, mMatrix3f * vec);
}

TEST_F(MatrixTest, Matrix4fIdentityMultiplication) {
    ASSERT_EQ(mMatrix4f, mMatrix4f * rt::cIdentity4f);
}

TEST_F(MatrixTest, IdentityByTupleMultiplication) {
    ASSERT_EQ(mTuple, rt::cIdentity4f * mTuple);
}

TEST_F(MatrixTest, TransposeMatrix) {
    const rt::Matrix4f result = {
        {1, 5, 9, 5}, {2, 6, 8, 4}, {3, 7, 7, 3}, {4, 8, 6, 2}};
    ASSERT_EQ(result, mMatrix4f.transposed());
}

TEST_F(MatrixTest, IdentityTranspose) {
    ASSERT_EQ(rt::cIdentity4f, rt::cIdentity4f.transposed());
}

TEST_F(MatrixTest, Determinant1) {
    const rt::Matrix1f matrix1f = {{4.567f}};
    ASSERT_FLOAT_EQ(matrix1f.determinant(), 4.567f);
}

TEST_F(MatrixTest, Determinant2f) {
    ASSERT_FLOAT_EQ(mMatrix2f.determinant(), 1);
}

TEST_F(MatrixTest, Submatrix3f) {
    const rt::Matrix2f result = {{1, -2}, {0, 1}};
    ASSERT_EQ(mMatrix3f.submatrix(0, 2), result);
}

TEST_F(MatrixTest, SubMatrix4f) {
    const rt::Matrix3f result = {{1, 3, 4}, {5, 7, 8}, {5, 3, 2}};
    ASSERT_EQ(mMatrix4f.submatrix(2, 1), result);
}

TEST_F(MatrixTest, MinorMatrix3f) {
    const float result = mMatrix3f.submatrix(1, 0).determinant();
    ASSERT_FLOAT_EQ(result, mMatrix3f.minor(1, 0));
}

TEST_F(MatrixTest, Cofactor3f) {
    ASSERT_FLOAT_EQ(5, mMatrix3f.minor(0, 0));
    ASSERT_FLOAT_EQ(5, mMatrix3f.cofactor(0, 0));
    ASSERT_FLOAT_EQ(5, mMatrix3f.minor(1, 0));
    ASSERT_FLOAT_EQ(-5, mMatrix3f.cofactor(1, 0));
}

TEST_F(MatrixTest, Determinant3f) {
    ASSERT_FLOAT_EQ(5, mMatrix3f.cofactor(0, 0));
    ASSERT_FLOAT_EQ(-1, mMatrix3f.cofactor(0, 1));
    ASSERT_FLOAT_EQ(1, mMatrix3f.cofactor(0, 2));

    ASSERT_FLOAT_EQ(-20, mMatrix3f.determinant());
}

TEST_F(MatrixTest, Determinant4f) {
    ASSERT_FLOAT_EQ(3, mMatrix4fB.cofactor(0, 0));
    ASSERT_FLOAT_EQ(10, mMatrix4fB.cofactor(0, 1));
    ASSERT_FLOAT_EQ(-17, mMatrix4fB.cofactor(0, 2));
    ASSERT_FLOAT_EQ(12, mMatrix4fB.cofactor(0, 3));

    ASSERT_FLOAT_EQ(6, mMatrix4fB.determinant());
}

TEST_F(MatrixTest, IsInvertible) {
    ASSERT_FLOAT_EQ(-2120, mInvertibleMatrix.determinant());
    ASSERT_TRUE(mInvertibleMatrix.isInvertible());

    ASSERT_FLOAT_EQ(0, mNonInvertibleMatrix.determinant());
    ASSERT_FALSE(mNonInvertibleMatrix.isInvertible());
}

TEST_F(MatrixTest, InvertMatrix4f) {
    const rt::Matrix4f result = mInvertibleMatrix.inverted().inverted();

    ASSERT_EQ(result, mInvertibleMatrix);
}

TEST_F(MatrixTest, MultiplyProductByFactorInverse) {
    const rt::Matrix4f product = mMatrix4fB * mInvertibleMatrix;
    ASSERT_EQ(product * mInvertibleMatrix.inverted(), mMatrix4fB);
}

TEST_F(MatrixTest, IdentityMatrixInverted) {
    ASSERT_EQ(rt::cIdentity4f, rt::cIdentity4f.inverted());
}

TEST_F(MatrixTest, MultiplyMatrixByItsInverse) {
    const auto result = mInvertibleMatrix * mInvertibleMatrix.inverted();
    ASSERT_EQ(result, rt::cIdentity4f);
}

TEST_F(MatrixTest, TransposedInvertedVsInvertedTransposed) {
    const auto transposedInversed = mInvertibleMatrix.transposed().inverted();
    const auto invertedTransposed = mInvertibleMatrix.inverted().transposed();

    ASSERT_EQ(transposedInversed, invertedTransposed);
}