#include <gtest/gtest.h>

#include <cmath>

#include "common/Constants.hpp"
#include "common/Helpers.hpp"
#include "types/Point.hpp"
#include "types/Vector.hpp"

TEST(LiteralTest, FloatingPointValues) {
    ASSERT_FLOAT_EQ(10_f, 10.0f);
    ASSERT_FLOAT_EQ(-10_f, -10.0f);
}

TEST(CreatingPoint, PointCoefficient) {
    const auto a = rt::makePoint(4.3f, -4.2, 3.1);
    ASSERT_FLOAT_EQ(a.x, 4.3f);
    ASSERT_FLOAT_EQ(a.y, -4.2f);
    ASSERT_FLOAT_EQ(a.z, 3.1f);
    ASSERT_FLOAT_EQ(a.w, 1.0f);
}

TEST(CreatingPoint, ComparePointAndTuple) {
    const auto a = rt::makePoint(4.3f, -4.2f, 3.1f);
    const auto t = rt::Tuple{4.3f, -4.2f, 3.1f, 1.0f};
    ASSERT_EQ(a, t);
}

TEST(CreatingVector, VectorCoefficient) {
    const auto a = rt::makeVector(4.3f, -4.2f, 3.1f);
    ASSERT_FLOAT_EQ(a.x, 4.3f);
    ASSERT_FLOAT_EQ(a.y, -4.2f);
    ASSERT_FLOAT_EQ(a.z, 3.1f);
    ASSERT_FLOAT_EQ(a.w, 0.0f);
}

TEST(CreatingVector, CompareVectorAndTuple) {
    const auto a = rt::makeVector(4.3f, -4.2f, 3.1f);
    const auto t = rt::Tuple{4.3f, -4.2f, 3.1f, 0.0f};
    ASSERT_EQ(a, t);
}

class AritmethicsOnTuples : public ::testing::Test {
   protected:
    AritmethicsOnTuples()
        : mPoint1(rt::makePoint(3.0f, -2.0f, 5.0f)),
          mPoint2(rt::makePoint(-2.0f, 3.0f, 1.0f)),
          mVec1(rt::makeVector(-2.0f, 3.0f, 1.0f)),
          mVec2(rt::makeVector(3.0f, -2.0f, 5.0f)) {}
    ~AritmethicsOnTuples() {}

   protected:
    const rt::Point mPoint1;
    const rt::Point mPoint2;
    const rt::Vector mVec1;
    const rt::Vector mVec2;
};

TEST_F(AritmethicsOnTuples, Adding) {
    const auto pointPlusVec = rt::makePoint(1, 1, 6);
    ASSERT_EQ(mPoint1 + mVec1, pointPlusVec);

    const auto vecPlusVec = rt::makeVector(1, 1, 6);
    ASSERT_EQ(vecPlusVec, mVec1 + mVec2);
}

TEST_F(AritmethicsOnTuples, Subtracting) {
    const auto pointMinusPoint = rt::makeVector(5, -5, 4);
    ASSERT_EQ(pointMinusPoint, mPoint1 - mPoint2);

    const auto pointMinusVec = rt::makePoint(5, -5, 4);
    ASSERT_EQ(pointMinusVec, mPoint1 - mVec1);

    const auto vecMinusVec = rt::makeVector(-5, 5, -4);
    ASSERT_EQ(vecMinusVec, mVec1 - mVec2);
}

TEST_F(AritmethicsOnTuples, Negating) {
    const auto negatedVec = rt::makeVector(2.0f, -3.0f, -1.0f);
    ASSERT_EQ(negatedVec, -mVec1);
}

TEST_F(AritmethicsOnTuples, MultiplyingByScalar) {
    const auto scalar = 3.0f;
    const auto multiplied = rt::makeVector(-6, 9, 3);
    ASSERT_EQ(mVec1 * scalar, multiplied);

    const auto fraction = 0.5f;
    const auto multipliedByFraction = rt::makeVector(-1, 1.5f, 0.5f);
    ASSERT_EQ(mVec1 * fraction, multipliedByFraction);
}

TEST(VectorOperations, Magnitude) {
    const auto vecX = rt::makeVector(1, 0, 0);
    const auto vecY = rt::makeVector(0, 1, 0);
    const auto vecZ = rt::makeVector(0, 0, 1);
    const auto positiveVec = rt::makeVector(1, 2, 3);
    const auto negativeVec = rt::makeVector(-1, -2, -3);

    ASSERT_FLOAT_EQ(vecX.magnitude(), 1.0f);
    ASSERT_FLOAT_EQ(vecY.magnitude(), 1.0f);
    ASSERT_FLOAT_EQ(vecZ.magnitude(), 1.0f);
    ASSERT_FLOAT_EQ(positiveVec.magnitude(), std::sqrt(14.0f));
    ASSERT_FLOAT_EQ(negativeVec.magnitude(), std::sqrt(14.0f));
}

TEST(VectorOperations, Normalize) {
    const auto vecX = rt::makeVector(4, 0, 0);
    const auto vecY = rt::makeVector(0, 5, 0);
    const auto vecZ = rt::makeVector(0, 0, 6);
    const auto vecA = rt::makeVector(1, 2, 3);

    ASSERT_EQ(vecX.normalized(), rt::makeVector(1, 0, 0));
    ASSERT_EQ(vecY.normalized(), rt::makeVector(0, 1, 0));
    ASSERT_EQ(vecZ.normalized(), rt::makeVector(0, 0, 1));
    ASSERT_EQ(vecA.normalized(),
              rt::makeVector(1.0f / sqrt(14.0f), 2.0f / sqrt(14.0f),
                             3.0f / sqrt(14.0f)));
}

TEST(VectorOperations, DotProduct) {
    const auto vecA = rt::makeVector(1, 2, 3);
    const auto vecB = rt::makeVector(2, 3, 4);

    ASSERT_FLOAT_EQ(vecA.dot(vecB), 20.0f);
}

TEST(VectorOperations, CrossProduct) {
    const auto vecA = rt::makeVector(1, 2, 3);
    const auto vecB = rt::makeVector(2, 3, 4);

    ASSERT_EQ(vecA.cross(vecB), rt::makeVector(-1, 2, -1));
    ASSERT_EQ(vecB.cross(vecA), rt::makeVector(1, -2, 1));
}

TEST(ReflectingTest, ApproachingAt45Deg) {
    const auto vec = rt::makeVector(1.0f, -1.0f, 0.0f);
    const auto normal = rt::makeVector(0.0f, 1.0f, 0.0f);
    const auto reflected = vec.reflected(normal);
    ASSERT_EQ(reflected, rt::makeVector(1.0f, 1.0f, 0.0f));
}

TEST(ReflectingTest, SlantedSurface) {
    const auto vec = rt::makeVector(0.0f, -1.0f, 0.0f);
    const auto normal = rt::makeVector(rt::cSqrt2/2.0f, rt::cSqrt2/2.0f, 0.0f);
    const auto reflected = vec.reflected(normal);
    ASSERT_EQ(reflected, rt::makeVector(1.0f, 0.0f, 0.0f));
}