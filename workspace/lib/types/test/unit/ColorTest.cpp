#include <gtest/gtest.h>

#include <types/Color.hpp>

TEST(CreatingColor, ColorComponents) {
    const auto color = rt::makeColor(-0.5, 0.4, 1.7);
    ASSERT_FLOAT_EQ(color.red(), -0.5f);
    ASSERT_FLOAT_EQ(color.green(), 0.4f);
    ASSERT_FLOAT_EQ(color.blue(), 1.7f);
}

class ColorOperations : public ::testing::Test {
   protected:
    ColorOperations()
        : color1(rt::makeColor(0.9, 0.6, 0.75)),
          color2(rt::makeColor(0.7, 0.1, 0.25)) {}
    ~ColorOperations() {}

   protected:
    const rt::Color color1;
    const rt::Color color2;
};

TEST_F(ColorOperations, Adding) {
    const auto result = rt::makeColor(1.6, 0.7, 1.0);
    const auto result2 = color1 + color2;
    std::cout << result2;
    ASSERT_EQ(result, result2);
}

TEST_F(ColorOperations, Subtracting) {
    const auto result = rt::makeColor(0.2, 0.5, 0.5);
    ASSERT_EQ(result, color1 - color2);
}

TEST_F(ColorOperations, MultiplyingByScalar) {
    const auto result = rt::makeColor(1.8, 1.2, 1.5);
    const auto scalar = 2.0f;
    ASSERT_EQ(result, color1 * scalar);
}

TEST_F(ColorOperations, HadamardProduct) {
    const auto result = rt::makeColor(0.63, 0.06, 0.1875);
    ASSERT_EQ(result, color1.hadamard(color2));
}