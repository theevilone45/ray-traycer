#include <gtest/gtest.h>

#include <types/Canvas.hpp>
#include <types/Color.hpp>

TEST(CreatingCanvas, CorrectColorsAndDimentions) {
    const auto canvas = rt::makeCanvas(10, 20);
    ASSERT_EQ(canvas.width, 10);
    ASSERT_EQ(canvas.height, 20);

    for (const auto& pixel : canvas.pixels) {
        ASSERT_EQ(pixel, rt::color::Black);
    }
}

TEST(DrawingOnCanvas, SinglePixel) {
    auto canvas = rt::makeCanvas(10, 20);
    const auto color = rt::color::Red;

    canvas(1, 2) = color;

    ASSERT_EQ(canvas(1, 2), color);
}