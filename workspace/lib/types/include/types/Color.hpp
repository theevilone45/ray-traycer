#ifndef WORKSPACE_LIB_TYPES_INCLUDE_TYPES_COLOR
#define WORKSPACE_LIB_TYPES_INCLUDE_TYPES_COLOR

#include "types/Tuple.hpp"

namespace rt {

using Color = Tuple;
// color has w == 0.0
Color makeColor(float r, float g, float b);

namespace color {
const auto Black = makeColor(0, 0, 0);
const auto Red = makeColor(1, 0, 0);
const auto Green = makeColor(0, 1, 0);
const auto Blue = makeColor(0, 0, 1);
const auto White = makeColor(1, 1, 1);
}   // namespace color

}   // namespace rt

#endif /* WORKSPACE_LIB_TYPES_INCLUDE_TYPES_COLOR */
