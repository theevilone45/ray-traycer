#ifndef WORKSPACE_LIB_TYPES_INCLUDE_TUPLE
#define WORKSPACE_LIB_TYPES_INCLUDE_TUPLE

#include <cmath>
#include <ostream>

namespace rt {

struct Tuple {
    float x;
    float y;
    float z;
    float w;

    bool operator==(const Tuple& rhs) const;

    Tuple operator+(const Tuple& rhs) const;
    Tuple operator-(const Tuple& rhs) const;
    Tuple operator-() const;

    Tuple operator*(float scalar) const;
    Tuple operator/(float scalar) const;

    float magnitude() const;
    Tuple normalized() const;
    Tuple reflected(const Tuple& normal) const;
    Tuple hadamard(const Tuple& rhs) const;

    float dot(const Tuple& rhs) const;
    Tuple cross(const Tuple& rhs) const;

    Tuple(float x, float y, float z, float w);
    Tuple();

    const float& red() const;
    const float& green() const;
    const float& blue() const;
    float& red();
    float& green();
    float& blue();
};

std::ostream& operator<<(std::ostream& os, const Tuple& tuple);

}   // namespace rt

#endif /* WORKSPACE_LIB_TYPES_INCLUDE_TUPLE */
