#ifndef WORKSPACE_LIB_TYPES_INCLUDE_TYPES_MATRIX
#define WORKSPACE_LIB_TYPES_INCLUDE_TYPES_MATRIX

#include <array>
#include <cassert>
#include <cstdint>
#include <iomanip>
#include <optional>
#include <ostream>

#include "common/Helpers.hpp"
#include "types/Tuple.hpp"

namespace rt {

// TODO: reasearch and add good concepts to ValueType
// TODO: try to add more cache to matrix class if necessary

template <uint32_t Size, typename ValueType>
class Matrix {
   public:
    using MatrixType = Matrix<Size, ValueType>;
    using MatrixMinorType = Matrix<Size, ValueType>;

    using iterator = typename std::array<ValueType, Size>::iterator;
    using const_iterator = typename std::array<ValueType, Size>::const_iterator;

    constexpr const static uint32_t cWidth = Size;
    constexpr const static uint32_t cHeight = Size;

    constexpr Matrix() : mDeterminantCache(std::nullopt), mData{} {};

    Matrix(std::initializer_list<std::initializer_list<ValueType>> values)
        : mDeterminantCache(std::nullopt) {
        uint32_t idx = 0;
        for (const auto& row : values) {
            for (const auto& cell : row) {
                mData[idx++] = cell;
            }
        }
    }

    ValueType& operator()(uint32_t row, uint32_t column) {
        // cache need to be invalidated after writing to matrix
        mDeterminantCache = std::nullopt;
        return mData.at(row * Size + column);
    }

    const ValueType operator()(uint32_t row, uint32_t column) const {
        return mData.at(row * Size + column);
    }

    bool operator==(const Matrix<Size, ValueType>& rhs) const {
        using common::isEqual;
        size_t idx = 0;
        for (const auto& elem : mData) {
            if (!isEqual(elem, rhs.mData.at(idx++))) {
                return false;
            }
        }
        return true;
    }

    Matrix<Size, ValueType> operator*(
        const Matrix<Size, ValueType>& rhs) const {
        Matrix<Size, ValueType> result{};

        for (uint32_t row = 0; row < cHeight; row++) {
            for (uint32_t col = 0; col < cWidth; col++) {
                result(row, col) = 0.0f;
                for (uint32_t idx = 0; idx < Size; idx++) {
                    result(row, col) +=
                        this->operator()(row, idx) * rhs(idx, col);
                }
            }
        }

        return result;
    }

    Tuple operator*(const Tuple& rhs) const {
        static_assert(Size == 3 || Size == 4);
        Tuple result = {0, 0, 0, 0};

        result.x = this->operator()(0, 0) * rhs.x +
                   this->operator()(0, 1) * rhs.y +
                   this->operator()(0, 2) * rhs.z;

        result.y = this->operator()(1, 0) * rhs.x +
                   this->operator()(1, 1) * rhs.y +
                   this->operator()(1, 2) * rhs.z;

        result.z = this->operator()(2, 0) * rhs.x +
                   this->operator()(2, 1) * rhs.y +
                   this->operator()(2, 2) * rhs.z;

        if constexpr (Size == 4) {
            result.x += this->operator()(0, 3) * rhs.w;
            result.y += this->operator()(1, 3) * rhs.w;
            result.z += this->operator()(2, 3) * rhs.w;

            result.w = this->operator()(3, 0) * rhs.x +
                       this->operator()(3, 1) * rhs.y +
                       this->operator()(3, 2) * rhs.z +
                       this->operator()(3, 3) * rhs.w;
        }

        return result;
    }

    Matrix<Size, ValueType> transposed() const {
        Matrix<Size, ValueType> transposed{};
        for (uint32_t row = 0; row < cHeight; row++) {
            for (uint32_t col = 0; col < cWidth; col++) {
                transposed(row, col) = this->operator()(col, row);
            }
        }
        return transposed;
    }

    constexpr static const Matrix<Size, ValueType> identity() {
        Matrix<Size, ValueType> result{};
        for (uint32_t idx = 0; idx < Size; idx++) {
            result.mData.at(idx * Size + idx) = static_cast<ValueType>(1);
        }
        return result;
    }

    ValueType cofactor(uint32_t row, uint32_t col) const {
        return minor(row, col) *
               static_cast<ValueType>((row + col) % 2 == 0 ? 1 : -1);
    }

    ValueType determinant() const {
        if (!mDeterminantCache) {
            mDeterminantCache =
                getDeterminant<ValueType, Matrix<Size, ValueType>>(*this);
        }
        return *mDeterminantCache;
    }

    ValueType minor(uint32_t row, uint32_t col) const {
        return getMinor<ValueType, Matrix<Size, ValueType>>(*this, row, col);
    }

    bool isInvertible() const {
        return determinant() != static_cast<ValueType>(0);
    }

    Matrix<Size - 1, ValueType> submatrix(uint32_t row, uint32_t col) const {
        Matrix<Size - 1, ValueType> result{};
        uint32_t newRow = 0;
        for (uint32_t r = 0; r < cHeight; r++) {
            uint32_t newCol = 0;
            if (r == row) {
                continue;
            }
            for (uint32_t c = 0; c < cWidth; c++) {
                if (c == col) {
                    continue;
                }
                result(newRow, newCol++) = this->operator()(r, c);
            }
            newRow++;
        }
        return result;
    }

    Matrix<Size, ValueType> inverted() const {
        assert(isInvertible());
        Matrix<Size, ValueType> result{};
        for (uint32_t row = 0; row < cHeight; row++) {
            for (uint32_t col = 0; col < cWidth; col++) {
                auto cof = cofactor(row, col);
                result(col, row) = cof / determinant();
            }
        }
        return result;
    }

   private:
    mutable std::optional<ValueType> mDeterminantCache;
    std::array<ValueType, cHeight * cWidth> mData;
};

using Matrix4f = Matrix<4, float>;
using Matrix3f = Matrix<3, float>;
using Matrix2f = Matrix<2, float>;
using Matrix1f = Matrix<1, float>;

constexpr const auto cIdentity4f = Matrix4f::identity();
constexpr const auto cIdentity3f = Matrix3f::identity();
constexpr const auto cIdentity2f = Matrix2f::identity();
constexpr const auto cIdentity1f = Matrix1f::identity();

template <uint32_t Size, typename ValueType>
std::ostream& operator<<(std::ostream& os,
                         const rt::Matrix<Size, ValueType>& matrix) {
    os << std::fixed << std::setprecision(5);
    os << "[matrix " << Size << " x " << Size << "]\n";
    for (uint32_t row = 0; row < Size; ++row) {
        os << "+";
        for (uint32_t col = 0; col < Size; ++col) {
            os << std::setw(14) << std::setfill('-') << "+";
        }
        os << "\n|";
        for (uint32_t col = 0; col < Size; ++col) {
            os << std::setw(12) << std::setfill(' ') << matrix(row, col)
               << " |";
        }
        os << '\n';
    }
    os << "+";
    for (uint32_t col = 0; col < Size; ++col) {
        os << std::setw(14) << std::setfill('-') << "+";
    }
    os << std::setfill(' ') << '\n';
    return os;
}

}   // namespace rt

#include "Matrix.hxx"

#endif /* WORKSPACE_LIB_TYPES_INCLUDE_TYPES_MATRIX */
