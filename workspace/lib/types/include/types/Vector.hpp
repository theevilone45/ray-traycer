#ifndef WORKSPACE_LIB_TYPES_INCLUDE_TYPES_VECTOR
#define WORKSPACE_LIB_TYPES_INCLUDE_TYPES_VECTOR

#include "types/Tuple.hpp"

namespace rt {
using Vector = Tuple;
// vector is tuple with w component eq 0.0
Vector makeVector(float x, float y, float z);
}   // namespace rt

#endif /* WORKSPACE_LIB_TYPES_INCLUDE_TYPES_VECTOR */
