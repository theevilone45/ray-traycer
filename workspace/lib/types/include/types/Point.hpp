#ifndef WORKSPACE_LIB_TYPES_INCLUDE_TYPES_POINT
#define WORKSPACE_LIB_TYPES_INCLUDE_TYPES_POINT

#include "types/Tuple.hpp"

namespace rt {

using Point = Tuple;
// point is tuple with w component eq 1.0
Point makePoint(float x, float y, float z);

}   // namespace rt

#endif /* WORKSPACE_LIB_TYPES_INCLUDE_TYPES_POINT */
