#ifndef WORKSPACE_LIB_TYPES_INCLUDE_TYPES_MATRIX_HXX
#define WORKSPACE_LIB_TYPES_INCLUDE_TYPES_MATRIX_HXX

#include "Matrix.hpp"

namespace rt {

namespace {

template <typename ValueType, typename MatrixType>
ValueType getDeterminant(const MatrixType& matrix) {
    ValueType result{};

    for (uint32_t idx = 0; idx < MatrixType::cWidth; idx++) {
        result += matrix(0, idx) * matrix.cofactor(0, idx);
    }

    return result;
}


template <>
[[maybe_unused]] float getDeterminant(const Matrix1f& matrix) {
    return matrix(0, 0);
}

template <>
[[maybe_unused]] float getDeterminant(const Matrix2f& matrix) {
    return matrix(0, 0) * matrix(1, 1) - matrix(0, 1) * matrix(1, 0);
}

template <typename ValueType, typename MatrixType>
ValueType getMinor(const MatrixType& matrix, uint32_t row, uint32_t col) {
    return matrix.submatrix(row, col).determinant();
}

template <>
[[maybe_unused]] float getMinor(const Matrix1f& matrix, [[maybe_unused]] uint32_t row,
               [[maybe_unused]] uint32_t col) {
    return matrix(0, 0);
}

}   // namespace

}   // namespace rt
#endif /* WORKSPACE_LIB_TYPES_INCLUDE_TYPES_MATRIX_HXX */
