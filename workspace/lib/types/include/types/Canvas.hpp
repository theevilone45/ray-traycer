#ifndef WORKSPACE_LIB_TYPES_INCLUDE_TYPES_CANVAS
#define WORKSPACE_LIB_TYPES_INCLUDE_TYPES_CANVAS

#include <cstdint>
#include <vector>

#include "types/Color.hpp"

namespace rt {

struct Canvas {
    const uint32_t width;
    const uint32_t height;

    std::vector<Color> pixels;

    Canvas(uint32_t width, uint32_t height);

    Color& operator()(uint32_t x, uint32_t y);
    const Color& operator()(uint32_t x, uint32_t y) const;
};

Canvas makeCanvas(uint32_t width, uint32_t height);

}   // namespace rt

#endif /* WORKSPACE_LIB_TYPES_INCLUDE_TYPES_CANVAS */
