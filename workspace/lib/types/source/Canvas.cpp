#include "types/Canvas.hpp"

#include "types/Color.hpp"

namespace rt {

Canvas::Canvas(uint32_t width, uint32_t height) : width(width), height(height) {
    pixels.resize(width * height, color::Black);
}

Canvas makeCanvas(uint32_t width, uint32_t height) {
    return Canvas(width, height);
}

Color& Canvas::operator()(uint32_t x, uint32_t y) {
    return pixels.at(y * width + x);
}

const Color& Canvas::operator()(uint32_t x, uint32_t y) const {
    return pixels.at(y * width + x);
}

}   // namespace rt