#include "types/Color.hpp"

namespace rt {
Color makeColor(float r, float g, float b) { return Color{r, g, b, 0.0}; }
}   // namespace rt
