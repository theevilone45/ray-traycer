#include "types/Point.hpp"

namespace rt {
Point makePoint(float x, float y, float z) { return Point{x, y, z, 1.0}; }
}   // namespace rt
