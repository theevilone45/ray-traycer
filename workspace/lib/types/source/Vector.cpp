#include "types/Vector.hpp"

namespace rt {
Vector makeVector(float x, float y, float z) { return Vector{x, y, z, 0.0}; }
}   // namespace rt
