set(SOURCES
    Tuple.cpp
    Color.cpp
    Vector.cpp
    Point.cpp
    Canvas.cpp
)

add_library(${TYPES_LIB} SHARED ${SOURCES})
target_include_directories(${TYPES_LIB} PUBLIC ../include ${COMMON_INCLUDE_DIR})