#include "types/Tuple.hpp"

#include <cassert>

#include "common/Helpers.hpp"

namespace rt {

bool Tuple::operator==(const Tuple& rhs) const {
    using common::isEqual;
    return isEqual(x, rhs.x) && isEqual(y, rhs.y) && isEqual(z, rhs.z) &&
           isEqual(w, rhs.w);
}

Tuple Tuple::operator+(const Tuple& rhs) const {
    return {
        x + rhs.x,
        y + rhs.y,
        z + rhs.z,
        w + rhs.w,
    };
}

Tuple Tuple::operator-(const Tuple& rhs) const {
    return {
        x - rhs.x,
        y - rhs.y,
        z - rhs.z,
        w - rhs.w,
    };
}

Tuple Tuple::operator-() const { return {-x, -y, -z, -w}; }

Tuple Tuple::operator*(float scalar) const {
    return {
        x * scalar,
        y * scalar,
        z * scalar,
        w * scalar,
    };
}

Tuple Tuple::operator/(float scalar) const {
    return {
        x / scalar,
        y / scalar,
        z / scalar,
        w / scalar,
    };
}

Tuple Tuple::reflected(const Tuple& normal) const {
    return *this - normal * 2 * this->dot(normal);
}

float Tuple::magnitude() const { return sqrt(x * x + y * y + z * z + w * w); }

Tuple Tuple::normalized() const {
    return {x / magnitude(), y / magnitude(), z / magnitude(), w / magnitude()};
}

Tuple Tuple::hadamard(const Tuple& rhs) const {
    return {x * rhs.x, y * rhs.y, z * rhs.z, w * rhs.w};
}

float Tuple::dot(const Tuple& rhs) const {
    return x * rhs.x + y * rhs.y + z * rhs.z + w * rhs.w;
}

Tuple Tuple::cross(const Tuple& rhs) const {
    assert(w == 0.0f);
    return {y * rhs.z - z * rhs.y, z * rhs.x - x * rhs.z, x * rhs.y - y * rhs.x,
            0.0f};
}

Tuple::Tuple(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}
Tuple::Tuple() : x{}, y{}, z{}, w{} {}

const float& Tuple::red() const { return x; }
const float& Tuple::green() const { return y; }
const float& Tuple::blue() const { return z; }
float& Tuple::red() { return x; }
float& Tuple::green() { return y; }
float& Tuple::blue() { return z; }

std::ostream& operator<<(std::ostream& os, const Tuple& tuple) {
    os << "[tuple] (" << tuple.x << " " << tuple.y << " " << tuple.z << " "
       << tuple.w << ")";
    return os;
}

}   // namespace rt