#ifndef WORKSPACE_LIB_COMMON_INCLUDE_INTERFACES_WORLDINTERFACE
#define WORKSPACE_LIB_COMMON_INCLUDE_INTERFACES_WORLDINTERFACE

#include <memory>
#include <optional>
#include <vector>

namespace rt {

class Precomputations;
class Object;
class PointLight;
class Tuple;
class Ray;

/**
 * @brief Definition of World's base class
 *
 */
class IWorld {
   public:
    using ObjectsContainer = std::vector<std::shared_ptr<Object>>;
    using Color = Tuple;
    using Point = Tuple;

   public:
    virtual const ObjectsContainer& objects() const = 0;
    virtual ObjectsContainer& objects() = 0;
    virtual const std::optional<PointLight>& light() const = 0;
    virtual std::optional<PointLight>& light() = 0;
    virtual Color shade(const Precomputations& comps,
                        uint8_t bounces) const = 0;
    virtual Color colorAt(const Ray& ray, uint8_t bounces) const = 0;
    virtual bool isInShadow(const Point& point) const = 0;
    virtual Color reflectedColor(const Precomputations& comps,
                                 uint8_t bounces) const = 0;
};
}   // namespace rt

#endif /* WORKSPACE_LIB_COMMON_INCLUDE_INTERFACES_WORLDINTERFACE */
