#ifndef WORKSPACE_LIB_COMMON_INCLUDE_COMMON_HELPERS
#define WORKSPACE_LIB_COMMON_INCLUDE_COMMON_HELPERS

#include <cmath>
#include <limits>

#include "common/Constants.hpp"

namespace rt::common {
constexpr bool isEqual(const float a, const float b) {
    constexpr const float epsilon = cFloatEpsilon;
    return std::abs(a - b) <= epsilon;
}
}   // namespace rt::common

constexpr float operator""_f(unsigned long long int value) {
    return static_cast<float>(value);
}

#endif /* WORKSPACE_LIB_COMMON_INCLUDE_COMMON_HELPERS */
