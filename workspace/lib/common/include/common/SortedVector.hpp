#ifndef WORKSPACE_LIB_COMMON_INCLUDE_COMMON_SORTEDVECTOR
#define WORKSPACE_LIB_COMMON_INCLUDE_COMMON_SORTEDVECTOR

#include <algorithm>
#include <vector>

namespace rt {

template <typename T, typename Compare = std::less<T>>
class SortedVector {
   public:
    using value_type = T;
    using iterator = typename std::vector<T>::iterator;
    using const_iterator = typename std::vector<T>::const_iterator;

    SortedVector() = default;
    SortedVector(const Compare& compare) : mCompare(compare) {}

    void insert(T value) {
        auto it = std::lower_bound(mData.begin(), mData.end(), value, mCompare);
        mData.insert(it, value);
    }

    const T& at(size_t idx) const { return mData.at(idx); }

    T& at(size_t idx) { return mData.at(idx); }

    iterator begin() { return mData.begin(); }
    const_iterator begin() const { return mData.begin(); }

    iterator end() { return mData.end(); }
    const_iterator end() const { return mData.end(); }

    size_t size() const { return mData.size(); }

   private:
    std::vector<T> mData;
    Compare mCompare;
};

}   // namespace rt

#endif /* WORKSPACE_LIB_COMMON_INCLUDE_COMMON_SORTEDVECTOR */
