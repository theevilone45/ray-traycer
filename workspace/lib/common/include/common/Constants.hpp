#ifndef WORKSPACE_LIB_COMMON_INCLUDE_COMMON_CONSTANTS
#define WORKSPACE_LIB_COMMON_INCLUDE_COMMON_CONSTANTS

#include <numbers>

namespace rt {
constexpr const float cPi = std::numbers::pi_v<float>;
constexpr const float cSqrt2 = std::numbers::sqrt2_v<float>;
constexpr const float cSqrt3 = std::numbers::sqrt3_v<float>;
constexpr const float cFloatEpsilon = 0.001f;
constexpr const float cPointOffsetEpsilon = 0.001f;
constexpr const uint8_t cMaxLightBounces = 5;
}   // namespace rt

#endif /* WORKSPACE_LIB_COMMON_INCLUDE_COMMON_CONSTANTS */
