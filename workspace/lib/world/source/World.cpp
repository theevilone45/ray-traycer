/*
##############################
# LibGen 1.0                 #
# Author: theevilone45       #
# Created: 11.05.2023        #
##############################
 */

#include "world/World.hpp"

#include "objects/Material.hpp"
#include "objects/Sphere.hpp"
#include "rays/Intersection.hpp"
#include "transformations/Transformations.hpp"

namespace rt {

const World::ObjectsContainer& World::objects() const { return mObjects; }
World::ObjectsContainer& World::objects() { return mObjects; }
const std::optional<PointLight>& World::light() const { return mLight; }
std::optional<PointLight>& World::light() { return mLight; }

Color World::shade(const Precomputations& comps, uint8_t bounces) const {
    bool inShadow = isInShadow(comps.overPoint);

    const auto surface = comps.object->material.lighting(
        *mLight, comps.overPoint, comps.eyeVec, comps.normalVec, inShadow);

    const auto reflected = reflectedColor(comps, bounces);

    return surface + reflected;
}

Color World::colorAt(const Ray& ray, uint8_t bounces) const {
    const auto xs = ray.intersect(this);
    const auto hit = rt::hit(xs);
    if (!hit) {
        return color::Black;
    }
    const auto comps = makePrecomputations(*hit, ray);
    return shade(comps, bounces);
}

bool World::isInShadow(const Point& point) const {
    assert(mLight);
    const auto vec = mLight->position - point;
    const auto distance = vec.magnitude();
    const auto direction = vec.normalized();
    const auto ray = rt::makeRay(point, direction);
    const auto xs = ray.intersect(this);
    const auto hit = rt::hit(xs);
    if (hit && hit->t < distance) {
        return true;
    }

    return false;
}

Color World::reflectedColor(const Precomputations& comps,
                            uint8_t bounces) const {
    if (bounces == 0 || comps.object->material.reflective == 0.0f) {
        return rt::color::Black;
    }
    const auto reflectedRay = makeRay(comps.overPoint, comps.reflectVec);
    const auto color = colorAt(reflectedRay, bounces - 1);
    return color * comps.object->material.reflective;
}

World makeEmptyWorld() { return World(); }

WorldBuilder& WorldBuilder::addObject(std::shared_ptr<Object> object) {
    mWorld.mObjects.push_back(object);
    return *this;
}
WorldBuilder& WorldBuilder::addLight(const PointLight& light) {
    if (mLightState == LightState::Added) {
        throw std::runtime_error("cannot add more than one light source");
    }
    mWorld.mLight = light;
    mLightState = LightState::Added;
    return *this;
}
const World& WorldBuilder::get() const { return mWorld; }

World makeDefaultWorld() {
    return WorldBuilder()
        .addObject(SphereBuilder()
                       .material(MaterialBuilder()
                                     .color(rt::makeColor(0.8f, 1.0f, 0.6f))
                                     .diffuse(0.7f)
                                     .specular(0.2f)
                                     .get())
                       .get())
        .addObject(
            SphereBuilder()
                .transformation(Transform4f().scale(0.5f, 0.5f, 0.5f).get())
                .get())
        .addLight(rt::makePointLight(rt::color::White,
                                     rt::makePoint(-10.0f, 10.0f, -10.0f)))
        .get();
}

}   // namespace rt
