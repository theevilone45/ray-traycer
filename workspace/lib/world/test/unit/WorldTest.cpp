#include <gtest/gtest.h>

#include <algorithm>

#include "objects/Light.hpp"
#include "objects/Material.hpp"
#include "objects/Sphere.hpp"
#include "rays/Ray.hpp"
#include "transformations/Transformations.hpp"
#include "types/Color.hpp"
#include "types/Point.hpp"
#include "world/World.hpp"

TEST(WorldTest, CreatingEmptyWorld) {
    const auto world = rt::makeEmptyWorld();
    ASSERT_TRUE(world.objects().empty());
    ASSERT_EQ(world.light(), std::nullopt);
}

bool contains(const rt::World& world, const rt::Sphere& sphere) {
    const auto found = std::find_if(
        std::begin(world.objects()), std::end(world.objects()),
        [sphere](const auto& elem) {
            const auto& sphereElem = static_cast<const rt::Sphere&>(*elem);
            return sphere == sphereElem;
        });
    return found != std::end(world.objects());
}

TEST(WorldTest, CreatingDefaultWorld) {
    const auto light = rt::makePointLight(rt::color::White,
                                          rt::makePoint(-10.0f, 10.0f, -10.0f));
    const auto sphere1 =
        rt::SphereBuilder()
            .material(rt::MaterialBuilder()
                          .color(rt::makeColor(0.8f, 1.0f, 0.6f))
                          .diffuse(0.7f)
                          .specular(0.2f)
                          .get())
            .get();
    const auto sphere2 =
        rt::SphereBuilder()
            .transformation(rt::Transform4f().scale(0.5f, 0.5f, 0.5f).get())
            .get();

    const auto world = rt::makeDefaultWorld();

    ASSERT_EQ(light, *world.light());
    ASSERT_TRUE(contains(world, *sphere1));
    ASSERT_TRUE(contains(world, *sphere2));
}

TEST(WorldTest, IntersectWorld) {
    const auto world = rt::makeDefaultWorld();
    const auto ray = rt::makeRay(rt::makePoint(0.0f, 0.0f, -5.0f),
                                 rt::makeVector(0.0f, 0.0f, 1.0f));

    const auto xs = ray.intersect(&world);

    ASSERT_EQ(xs.size(), 4);
    ASSERT_EQ(xs.at(0).t, 4.0f);
    ASSERT_EQ(xs.at(1).t, 4.5f);
    ASSERT_EQ(xs.at(2).t, 5.5f);
    ASSERT_EQ(xs.at(3).t, 6.0f);

    ASSERT_NE(xs.at(0).object, nullptr);
}

TEST(WorldTest, ShadingIntersection) {
    const auto world = rt::makeDefaultWorld();
    const auto ray = rt::makeRay(rt::makePoint(0.0f, 0.0f, -5.0f),
                                 rt::makeVector(0.0f, 0.0f, 1.0f));

    const auto shape = world.objects().at(0);
    const auto intersection = rt::makeIntersection(4.0, shape.get());
    const auto comps = rt::makePrecomputations(intersection, ray);
    const auto color = world.shade(comps);
    ASSERT_EQ(color, rt::makeColor(0.38066f, 0.47583f, 0.2855f));
}

TEST(WorldTest, ShadingIntersectionFromInside) {
    auto world = rt::makeDefaultWorld();
    world.light() =
        rt::makePointLight(rt::color::White, rt::makePoint(0.0f, 0.25f, 0.0f));
    const auto ray = rt::makeRay(rt::makePoint(0.0f, 0.0f, 0.0f),
                                 rt::makeVector(0.0f, 0.0f, 1.0f));
    const auto shape = world.objects().at(1);
    const auto intersection = rt::makeIntersection(0.5f, shape.get());
    const auto comps = rt::makePrecomputations(intersection, ray);
    const auto color = world.shade(comps);
    ASSERT_EQ(color, rt::makeColor(0.90498f, 0.90498f, 0.90498f));
}

TEST(WorldTest, ColorAtMissedRay) {
    const auto world = rt::makeDefaultWorld();
    const auto ray = rt::makeRay(rt::makePoint(0.0f, 0.0f, -5.0f),
                                 rt::makeVector(0.0f, 1.0f, 0.0f));
    const auto color = world.colorAt(ray);
    ASSERT_EQ(color, rt::color::Black);
}

TEST(WorldTest, ColorAtRay) {
    const auto world = rt::makeDefaultWorld();
    const auto ray = rt::makeRay(rt::makePoint(0.0f, 0.0f, -5.0f),
                                 rt::makeVector(0.0f, 0.0f, 1.0f));
    const auto color = world.colorAt(ray);
    ASSERT_EQ(color, rt::makeColor(0.38066f, 0.47583f, 0.2855f));
}

TEST(WorldShadowTest, NothingToCollide) {
    const auto world = rt::makeDefaultWorld();
    const auto point = rt::makePoint(0_f, 10_f, 0_f);
    ASSERT_FALSE(world.isInShadow(point));
}

TEST(WorldShadowTest, ObjectBetweenPointAndLight) {
    const auto world = rt::makeDefaultWorld();
    const auto point = rt::makePoint(10_f, -10_f, 10_f);
    ASSERT_TRUE(world.isInShadow(point));
}

TEST(WorldShadowTest, ObjectBehindLight) {
    const auto world = rt::makeDefaultWorld();
    const auto point = rt::makePoint(-20_f, 20_f, -20_f);
    ASSERT_FALSE(world.isInShadow(point));
}

TEST(WorldShadowTest, ObjectBehindPoint) {
    const auto world = rt::makeDefaultWorld();
    const auto point = rt::makePoint(-2_f, 2_f, -2_f);
    ASSERT_FALSE(world.isInShadow(point));
}

TEST(WorldShadowTest, ShadingInShadow) {
    const auto light =
        rt::makePointLight(rt::color::White, rt::makePoint(0_f, 0_f, -10_f));
    const auto sphere = std::make_shared<rt::Sphere>(rt::makeDefaultSphere());
    const auto sphere2 = rt::SphereBuilder()
                             .transformation(rt::translation(0_f, 0_f, 10_f))
                             .get();
    const auto world = rt::WorldBuilder()
                           .addLight(light)
                           .addObject(sphere)
                           .addObject(sphere2)
                           .get();

    const auto ray = rt::makeRay(rt::makePoint(0_f, 0_f, 5_f),
                                 rt::makeVector(0_f, 0_f, 1_f));
    const auto inter = rt::makeIntersection(4_f, sphere2.get());
    const auto comps = rt::makePrecomputations(inter, ray);
    const auto color = world.shade(comps);
    ASSERT_EQ(color, rt::makeColor(0.1f, 0.1f, 0.1f));
}

TEST(WorldReflectionsTest, NonReflectiveSurface) {
    auto world = rt::makeDefaultWorld();
    const auto ray = rt::makeRay(rt::makePoint(0.f, 0.f, 0.f),
                                 rt::makeVector(0.f, 0.f, 1.f));
    const auto shape = world.objects().at(0);
    shape->material.ambient = 1.0;
    const auto inter = rt::makeIntersection(1.f, shape.get());
    const auto comps = rt::makePrecomputations(inter, ray);
    const auto color = world.reflectedColor(comps);
    ASSERT_EQ(color, rt::color::Black);
}

TEST(WorldReflectionsTest, ReflectiveSurface) {
    auto world = rt::makeDefaultWorld();
    const auto ray =
        rt::makeRay(rt::makePoint(0.f, 0.f, -3.f),
                    rt::makeVector(0.f, -rt::cSqrt2 / 2.f, rt::cSqrt2 / 2.f));
    const auto shape =
        rt::PlaneBuilder()
            .material(rt::MaterialBuilder().reflective(0.5).get())
            .transformation(rt::translation(0.f, -1.f, 0.f))
            .get();
    world.objects().push_back(shape);
    const auto inter = rt::makeIntersection(rt::cSqrt2, shape.get());
    const auto comps = rt::makePrecomputations(inter, ray);
    const auto color = world.shade(comps);
    ASSERT_EQ(color, rt::makeColor(0.15, 0.15, 0.15));
}
