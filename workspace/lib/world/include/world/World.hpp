#ifndef WORKSPACE_LIB_WORLD_INCLUDE_WORLD_WORLD
#define WORKSPACE_LIB_WORLD_INCLUDE_WORLD_WORLD

#include <memory>
#include <optional>
#include <vector>

#include "common/interfaces/WorldInterface.hpp"
#include "objects/Light.hpp"
#include "objects/Object.hpp"
#include "rays/Ray.hpp"
#include "types/Color.hpp"

namespace rt {

class World : public IWorld {
   public:
    const ObjectsContainer& objects() const override;
    ObjectsContainer& objects() override;

    const std::optional<PointLight>& light() const override;
    std::optional<PointLight>& light() override;
    Color shade(const Precomputations& comps,
                uint8_t bounces = 1) const override;
    Color colorAt(const Ray& ray, uint8_t bounces = 1) const override;
    bool isInShadow(const Point& point) const override;
    Color reflectedColor(const Precomputations& comps,
                         uint8_t bounces = 1) const override;

   private:
    ObjectsContainer mObjects;
    std::optional<PointLight> mLight = std::nullopt;

    friend class WorldBuilder;
};

World makeEmptyWorld();

class WorldBuilder {
   private:
    enum class LightState : uint8_t {
        NotAdded,
        Added
    };

   public:
    WorldBuilder& addObject(std::shared_ptr<Object> object);
    WorldBuilder& addLight(const PointLight& light);

    const World& get() const;

   private:
    World mWorld = makeEmptyWorld();

    /// @brief right now world can only have one light source
    LightState mLightState = LightState::NotAdded;
};

/**
 * @brief this is a world containing:
 * - two spheres with different materials
 * - single point light
 * - single camera
 *
 * @return World
 */
World makeDefaultWorld();

}   // namespace rt

#endif /* WORKSPACE_LIB_WORLD_INCLUDE_WORLD_WORLD */
