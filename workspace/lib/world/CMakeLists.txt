
##############################
# LibGen 1.0                 #
# Author: theevilone45       #
# Created: 11.05.2023        #
##############################
add_subdirectory(source)

if(CMAKE_BUILD_TYPE STREQUAL "Debug")
    add_subdirectory(test)
endif()
