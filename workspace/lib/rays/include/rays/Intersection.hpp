#ifndef WORKSPACE_LIB_TYPES_INCLUDE_TYPES_INTERSECTION
#define WORKSPACE_LIB_TYPES_INCLUDE_TYPES_INTERSECTION

#include <ostream>
#include <vector>

#include "common/SortedVector.hpp"
#include "objects/Object.hpp"

namespace rt {

template <typename ObjectType>
struct Intersection {
    Intersection(float t, const ObjectType* object) : t(t), object(object) {}

    bool operator==(const Intersection& rhs) const {
        return rhs.t == t && (*rhs.object == *object);
    }

    float t;
    const ObjectType* object;
};

template <typename ObjectType>
std::ostream& operator<<(std::ostream& os,
                         const Intersection<ObjectType>& intersection) {
    os << "[intersection]\n"
       << "t: " << intersection.t << '\n'
       << "type: " << intersection.object->type << '\n';
    return os;
}

Intersection<Object> makeIntersection(float t, const Object* object);

template <typename ObjectType>
class IntersectionComparator {
   public:
    bool operator()(const Intersection<ObjectType>& a,
                    const Intersection<ObjectType>& b) const {
        return a.t < b.t;
    }
};

template <typename ObjectType>
using IntersectionContainer =
    rt::SortedVector<Intersection<ObjectType>,
                     IntersectionComparator<ObjectType>>;

/**
 * @brief
 *
 * @tparam ObjectType
 * @param intersections -> container with detected ray-object intersections
 * @return const Intersection<ObjectType>& -> iterator pointing to in
 */
template <typename ObjectType>
const Intersection<ObjectType>* hit(
    const IntersectionContainer<ObjectType>& intersections) {
    const auto it =
        std::find_if(intersections.begin(), intersections.end(),
                     [](const auto& value) { return value.t >= 0.0f; });

    if (it == intersections.end()) {
        return nullptr;
    }

    return &(*it);
}

}   // namespace rt

#endif /* WORKSPACE_LIB_TYPES_INCLUDE_TYPES_INTERSECTION */
