#ifndef WORKSPACE_LIB_RAYS_INCLUDE_RAYS_RAY
#define WORKSPACE_LIB_RAYS_INCLUDE_RAYS_RAY

#include <array>
#include <map>
#include <optional>
#include <vector>

#include "objects/Object.hpp"
#include "objects/Sphere.hpp"
#include "objects/Plane.hpp"
#include "rays/Intersection.hpp"
#include "transformations/Transformations.hpp"
#include "types/Point.hpp"
#include "types/Vector.hpp"
#include "common/interfaces/WorldInterface.hpp"

namespace rt {
class Ray {
   public:
    Ray() = default;
    Ray(const Point& origin, const Vector& direction)
        : origin(origin), direction(direction) {}

    Point position(float t) const;

    /**
     * @brief dispaches intersect into doIntersect methods based on
     * object.type
     *
     * @param object ptr to object absract class
     * @return IntersectionContainer<Object>
     */
    IntersectionContainer<Object> intersect(const Object* object) const;

    IntersectionContainer<Object> intersect(const IWorld* world) const;

    Ray transform(const Matrix4f& transform) const;
    Ray& transformInPlace(const Matrix4f& transform);

    bool operator==(const Ray& ray) const;

   public:
    Point origin{rt::makePoint(0.0f, 0.0f, 0.0f)};
    Vector direction{};

   private:
    IntersectionContainer<Object> doIntersect(const Sphere* sphere) const;
    IntersectionContainer<Object> doIntersect(const Plane* sphere) const;
};

Ray makeRay(const rt::Point& origin, const rt::Vector& direction);

class Precomputations {
    public:
    const float t;
    const Object* object;
    const Point point;
    const Vector eyeVec;
    const Vector normalVec;
    const bool inside;
    const Point overPoint;
    const Vector reflectVec;
};

Precomputations makePrecomputations(const Intersection<Object>& intersection,
                                    const Ray& ray);

}   // namespace rt

#endif /* WORKSPACE_LIB_RAYS_INCLUDE_RAYS_RAY */
