#include <gtest/gtest.h>

#include "common/Constants.hpp"
#include "objects/Plane.hpp"
#include "objects/Sphere.hpp"
#include "rays/Ray.hpp"
#include "transformations/Transformations.hpp"
#include "types/Tuple.hpp"

TEST(RayTest, CreatingRay) {
    const auto origin = rt::makePoint(1.0f, 2.0f, 3.0f);
    const auto direction = rt::makeVector(4.0f, 5.0f, 6.0f);

    const auto ray = rt::makeRay(origin, direction);
    ASSERT_EQ(origin, ray.origin);
    ASSERT_EQ(direction, ray.direction);
}

TEST(RayTest, ComputingPointFromDistance) {
    const auto ray = rt::makeRay(rt::makePoint(2.0f, 3.0f, 4.0f),
                                 rt::makeVector(1.0f, 0.0f, 0.0f));
    ASSERT_EQ(ray.position(0), rt::makePoint(2.0f, 3.0f, 4.0f));
    ASSERT_EQ(ray.position(1), rt::makePoint(3.0f, 3.0f, 4.0f));
    ASSERT_EQ(ray.position(-1), rt::makePoint(1.0f, 3.0f, 4.0f));
    ASSERT_EQ(ray.position(2.5), rt::makePoint(4.5f, 3.0f, 4.0f));
}

TEST(IntersectionTest, CreatingIntersection) {
    const auto sphere = rt::makeDefaultSphere();
    const auto intersection = rt::makeIntersection(3.5f, &sphere);
    ASSERT_EQ(intersection.t, 3.5f);
    ASSERT_EQ(*(intersection.object), sphere);
}

TEST(IntersectionTest, HitOffsetingPoint) {
    const auto ray = rt::makeRay(rt::makePoint(0_f, 0_f, -5_f),
                                 rt::makeVector(0_f, 0_f, 1_f));
    const auto sphere = rt::SphereBuilder()
                            .transformation(rt::translation(0_f, 0_f, 1_f))
                            .get();
    const auto inter = rt::makeIntersection(5_f, sphere.get());
    const auto comps = rt::makePrecomputations(inter, ray);
    ASSERT_LT(comps.overPoint.z, -rt::cPointOffsetEpsilon / 2_f);
}

class PrecomputationTest : public ::testing::Test {
   protected:
    const rt::Ray mRay = rt::makeRay(rt::makePoint(0.0f, 0.0f, -5.0f),
                                     rt::makeVector(0.0f, 0.0f, 1.0f));
    const rt::Sphere mSphere = rt::makeDefaultSphere();
    const rt::Intersection<rt::Object> mIntersection =
        rt::makeIntersection(4.0f, &mSphere);
    const rt::Precomputations mComps =
        rt::makePrecomputations(mIntersection, mRay);
};

TEST_F(PrecomputationTest, PrecomputingStateOfIntersection) {
    ASSERT_EQ(mComps.t, mIntersection.t);
    ASSERT_EQ(*mComps.object, *mIntersection.object);
    ASSERT_EQ(mComps.point, rt::makePoint(0.0f, 0.0f, -1.0f));
    ASSERT_EQ(mComps.eyeVec, rt::makeVector(0.0f, 0.0f, -1.0f));
    ASSERT_EQ(mComps.normalVec, rt::makeVector(0.0f, 0.0f, -1.0f));
}

TEST_F(PrecomputationTest, PrecomputingIntersectionOutsideObject) {
    ASSERT_FALSE(mComps.inside);
}

TEST(ReflectionsTest, PrecomputationOfReflections) {
    const auto shape = rt::makeDefaultPlane();
    const auto ray =
        rt::makeRay(rt::makePoint(0.f, 1.f, -1.f),
                    rt::makeVector(0.f, -rt::cSqrt2 / 2.f, rt::cSqrt2 / 2.f));
    const auto inter = rt::makeIntersection(rt::cSqrt2, &shape);
    const auto comps = rt::makePrecomputations(inter, ray);
    ASSERT_EQ(comps.reflectVec,
              rt::makeVector(0.f, rt::cSqrt2 / 2.f, rt::cSqrt2 / 2.f));
}

TEST(RayTest, RayWithSphereIntersection) {
    const auto ray = rt::makeRay(rt::makePoint(0.0f, 0.0f, -5.0f),
                                 rt::makeVector(0.0f, 0.0f, 1.0f));
    const auto sphere = rt::makeDefaultSphere();
    const auto intersections = ray.intersect(&sphere);

    ASSERT_EQ(intersections.size(), 2);
    ASSERT_FLOAT_EQ(intersections.at(0).t, 4.0);
    ASSERT_FLOAT_EQ(intersections.at(1).t, 6.0);
    ASSERT_EQ(*(intersections.at(0).object), sphere);
    ASSERT_EQ(*(intersections.at(1).object), sphere);
}

TEST(RayTest, RayWithSphereSingleIntersection) {
    const auto ray = rt::makeRay(rt::makePoint(0.0f, 1.0f, -5.0f),
                                 rt::makeVector(0.0f, 0.0f, 1.0f));
    const auto sphere = rt::makeDefaultSphere();
    const auto intersections = ray.intersect(&sphere);

    ASSERT_EQ(intersections.size(), 2);
    ASSERT_FLOAT_EQ(intersections.at(0).t, 5.0);
    ASSERT_FLOAT_EQ(intersections.at(1).t, 5.0);
}

TEST(RayTest, RayWithSphereMiss) {
    const auto ray = rt::makeRay(rt::makePoint(0.0f, 2.0f, -5.0f),
                                 rt::makeVector(0.0f, 0.0f, 1.0f));
    const auto sphere = rt::makeDefaultSphere();
    const auto intersections = ray.intersect(&sphere);

    ASSERT_EQ(intersections.size(), 0);
}

TEST(RayTest, RayOriginInsideSphere) {
    const auto ray = rt::makeRay(rt::makePoint(0.0f, 0.0f, 0.0f),
                                 rt::makeVector(0.0f, 0.0f, 1.0f));
    const auto sphere = rt::makeDefaultSphere();
    const auto intersections = ray.intersect(&sphere);

    ASSERT_EQ(intersections.size(), 2);
    ASSERT_FLOAT_EQ(intersections.at(0).t, -1.0);
    ASSERT_FLOAT_EQ(intersections.at(1).t, 1.0);
}

TEST(RayTest, SphereBehindRay) {
    const auto ray = rt::makeRay(rt::makePoint(0.0f, 0.0f, 5.0f),
                                 rt::makeVector(0.0f, 0.0f, 1.0f));
    const auto sphere = rt::makeDefaultSphere();
    const auto intersections = ray.intersect(&sphere);

    ASSERT_EQ(intersections.size(), 2);
    ASSERT_FLOAT_EQ(intersections.at(0).t, -6.0);
    ASSERT_FLOAT_EQ(intersections.at(1).t, -4.0);
}

TEST(HitTest, AllPositiveIntersections) {
    const auto sphere = rt::makeDefaultSphere();
    rt::IntersectionContainer<rt::Object> intersections;

    const auto inter1 = rt::makeIntersection(1, &sphere);
    const auto inter2 = rt::makeIntersection(2, &sphere);

    intersections.insert(inter1);
    intersections.insert(inter2);
    const auto hit = rt::hit(intersections);

    ASSERT_TRUE(hit);
    ASSERT_EQ(*hit, inter1);
}

TEST(HitTest, PositiveAndNegativeIntersections) {
    const auto sphere = rt::makeDefaultSphere();
    rt::IntersectionContainer<rt::Object> intersections;
    const auto inter1 = rt::makeIntersection(-1, &sphere);
    const auto inter2 = rt::makeIntersection(1, &sphere);
    intersections.insert(inter1);
    intersections.insert(inter2);
    auto hit = rt::hit(intersections);
    ASSERT_TRUE(hit);
    ASSERT_EQ(*hit, inter2);
}

TEST(HitTest, AllNegativeIntersections) {
    const auto sphere = rt::makeDefaultSphere();
    rt::IntersectionContainer<rt::Object> intersections;
    const auto inter1 = rt::makeIntersection(-1, &sphere);
    const auto inter2 = rt::makeIntersection(-2, &sphere);
    intersections.insert(inter1);
    intersections.insert(inter2);
    auto hit = rt::hit(intersections);
    ASSERT_FALSE(hit);
}

TEST(HitTest, ALotOfIntersections) {
    const auto sphere = rt::makeDefaultSphere();
    rt::IntersectionContainer<rt::Object> intersections;
    const auto inter1 = rt::makeIntersection(5, &sphere);
    const auto inter2 = rt::makeIntersection(7, &sphere);
    const auto inter3 = rt::makeIntersection(-3, &sphere);
    const auto inter4 = rt::makeIntersection(2, &sphere);
    intersections.insert(inter1);
    intersections.insert(inter2);
    intersections.insert(inter3);
    intersections.insert(inter4);
    auto hit = rt::hit(intersections);
    ASSERT_TRUE(hit);
    ASSERT_EQ(*hit, inter4);
}

TEST(RayTransformTest, Translation) {
    auto ray = rt::makeRay(rt::makePoint(1.0f, 2.0f, 3.0f),
                           rt::makeVector(0.0f, 1.0f, 0.0f));
    const auto translation = rt::translation(3.0f, 4.0f, 5.0f);
    const auto ray2 = ray.transform(translation);
    const auto& rayRef = ray.transformInPlace(translation);
    ASSERT_EQ(ray2, rt::makeRay(rt::makePoint(4.0f, 6.0f, 8.0f),
                                rt::makeVector(0.0f, 1.0f, 0.0f)));
    ASSERT_EQ(rayRef, ray2);
}

TEST(RayTransformTest, Scaling) {
    const auto ray = rt::makeRay(rt::makePoint(1.0f, 2.0f, 3.0f),
                                 rt::makeVector(0.0f, 1.0f, 0.0f));
    const auto scaling = rt::scaling(2.0f, 3.0f, 4.0f);
    const auto ray2 = ray.transform(scaling);

    ASSERT_EQ(ray2, rt::makeRay(rt::makePoint(2.0f, 6.0f, 12.0f),
                                rt::makeVector(0.0f, 3.0f, 0.0f)));
}

TEST(SphereTansformTest, DefaultTransformation) {
    const auto sphere = rt::makeDefaultSphere();
    ASSERT_EQ(sphere.transformation, rt::cIdentity4f);
}

TEST(SphereTransformTest, IntersectRayWithScaledSphere) {
    auto sphere = rt::makeDefaultSphere();
    const auto ray = rt::makeRay(rt::makePoint(0.0f, 0.0f, -5.0f),
                                 rt::makeVector(0.0f, 0.0f, 1.0f));

    sphere.transformation = rt::scaling(2.0f, 2.0f, 2.0f);
    const auto xs = ray.intersect(&sphere);
    ASSERT_EQ(xs.size(), 2);
    ASSERT_FLOAT_EQ(xs.at(0).t, 3.0f);
    ASSERT_FLOAT_EQ(xs.at(1).t, 7.0f);
}

TEST(SphereTransformTest, IntersectRayWithTranslatedSphere) {
    auto sphere = rt::makeDefaultSphere();
    const auto ray = rt::makeRay(rt::makePoint(0.0f, 0.0f, -5.0f),
                                 rt::makeVector(0.0f, 0.0f, 1.0f));
    sphere.transformation = rt::translation(5.0f, 0.0f, 0.0f);
    const auto xs = ray.intersect(&sphere);
    ASSERT_EQ(xs.size(), 0);
}

TEST(PlaneIntersectionsTest, RayParallelToPlane) {
    const auto plane = rt::makeDefaultPlane();
    const auto ray = rt::makeRay(rt::makePoint(0.f, 5.f, 0.f),
                                 rt::makeVector(1.f, 0.f, 0.f));
    const auto xs = ray.intersect(&plane);
    ASSERT_EQ(xs.size(), 0);
}

TEST(PlaneIntersectionsTest, RayCoplanarWithPlane) {
    const auto plane = rt::makeDefaultPlane();
    const auto ray = rt::makeRay(rt::makePoint(0.f, 0.f, 0.f),
                                 rt::makeVector(1.f, 0.f, 0.f));
    const auto xs = ray.intersect(&plane);
    ASSERT_EQ(xs.size(), 0);
}

TEST(PlaneIntersectionsTest, RayIntersectingPlane) {
    const auto plane = rt::makeDefaultPlane();
    const auto ray = rt::makeRay(rt::makePoint(0.f, 1.f, 0.f),
                                 rt::makeVector(0.f, -1.f, 0.f));
    const auto xs = ray.intersect(&plane);
    ASSERT_EQ(xs.size(), 1);
    ASSERT_EQ(xs.at(0).t, 1.0f);
    ASSERT_EQ(*(xs.at(0).object), plane);
}