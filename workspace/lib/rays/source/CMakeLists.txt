
##############################
# LibGen 1.0                 #
# Author: theevilone45       #
# Created: 04.05.2023        #
##############################

set(SOURCES
    Ray.cpp
    Intersection.cpp
)

add_library(${RAYS_LIB} SHARED ${SOURCES})
target_link_libraries(${RAYS_LIB} PUBLIC ${TYPES_LIB} ${OBJECTS_LIB} ${TRANSFORMATIONS_LIB})
target_include_directories(${RAYS_LIB} PUBLIC ../include ${COMMON_INCLUDE_DIR})
