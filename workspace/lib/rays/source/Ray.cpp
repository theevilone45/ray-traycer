#include "rays/Ray.hpp"

namespace rt {

Point Ray::position(float t) const { return origin + direction * t; }

IntersectionContainer<Object> Ray::doIntersect(const Sphere* sphere) const {
    const auto transformedRay = transform(sphere->transformation.inverted());
    const auto sphereToRayOrigin = transformedRay.origin - sphere->origin;
    const auto a = transformedRay.direction.dot(transformedRay.direction);
    const auto b = 2 * transformedRay.direction.dot(sphereToRayOrigin);
    const auto c = sphereToRayOrigin.dot(sphereToRayOrigin) - 1;
    const auto discriminant = (b * b) - (4 * a * c);
    if (discriminant < 0) {
        return {};
    }

    IntersectionContainer<Object> result;

    result.insert(
        makeIntersection((-b - std::sqrt(discriminant)) / (2.0f * a), sphere));
    result.insert(
        makeIntersection((-b + std::sqrt(discriminant)) / (2.0f * a), sphere));
    return result;
}

IntersectionContainer<Object> Ray::doIntersect(const Plane* plane) const {
    if (std::abs(direction.y) < cFloatEpsilon) {
        return {};
    }
    IntersectionContainer<Object> result;
    result.insert(makeIntersection(-origin.y / direction.y, plane));
    return result;
}

Ray Ray::transform(const rt::Matrix4f& transform) const {
    return makeRay(transform * origin, transform * direction);
}

Ray& Ray::transformInPlace(const rt::Matrix4f& transform) {
    origin = transform * origin;
    direction = transform * direction;
    return *this;
}

Ray makeRay(const rt::Point& origin, const rt::Vector& direction) {
    return Ray(origin, direction);
}

bool Ray::operator==(const Ray& ray) const {
    return ray.origin == origin && ray.direction == direction;
}

IntersectionContainer<Object> Ray::intersect(const Object* object) const {
    switch (object->type) {
        case ObjectTypeEnum::Sphere: {
            const auto* sphere = static_cast<const Sphere*>(object);
            return doIntersect(sphere);
        }
        case ObjectTypeEnum::Plane: {
            const auto* plane = static_cast<const Plane*>(object);
            return doIntersect(plane);
        }
        default:
            return {};
    }
}

IntersectionContainer<Object> Ray::intersect(const IWorld* world) const {
    IntersectionContainer<Object> result = {};
    for (const auto& obj : world->objects()) {
        const auto xs = intersect(obj.get());
        for (const auto& x : xs) {
            result.insert(x);
        }
    }
    return result;
}

Precomputations makePrecomputations(const Intersection<Object>& intersection,
                                    const Ray& ray) {
    const auto t = intersection.t;
    const auto* object = intersection.object;
    const auto point = ray.position(t);
    const auto eyeVec = -ray.direction;
    auto normalVec = object->normal(point);
    bool inside = false;

    if (normalVec.dot(eyeVec) < 0) {
        normalVec = -normalVec;
        inside = true;
    }

    const auto overPoint = point + (normalVec * cPointOffsetEpsilon);
    const auto reflectVec = ray.direction.reflected(normalVec);

    return {t, object, point, eyeVec, normalVec, inside, overPoint, reflectVec};
}

}   // namespace rt
