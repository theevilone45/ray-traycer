#include "rays/Intersection.hpp"

namespace rt {

Intersection<Object> makeIntersection(float t, const Object* object) {
    return Intersection<Object>(t, object);
}

}   // namespace rt
