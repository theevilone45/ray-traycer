#ifndef WORKSPACE_LIB_OBJECTS_INCLUDE_OBJECTS_LIGHT
#define WORKSPACE_LIB_OBJECTS_INCLUDE_OBJECTS_LIGHT

#include "types/Color.hpp"
#include "types/Point.hpp"
#include "types/Vector.hpp"

namespace rt {

struct PointLight {
    PointLight() = default;
    PointLight(const Color& intensity, const Vector& position);

    Color intensity{color::White};
    Point position{rt::makePoint(0.0f, 0.0f, 0.0f)};

    bool operator==(const PointLight& rhs) const;
};

PointLight makePointLight(const Color& intensity, const Vector& position);
PointLight makePointLight();

}   // namespace rt

#endif /* WORKSPACE_LIB_OBJECTS_INCLUDE_OBJECTS_LIGHT */
