#ifndef WORKSPACE_LIB_OBJECTS_INCLUDE_OBJECTS_PLANE
#define WORKSPACE_LIB_OBJECTS_INCLUDE_OBJECTS_PLANE

#include <ostream>
#include <memory>

#include "objects/Object.hpp"
#include "types/Vector.hpp"
#include "types/Matrix.hpp"

namespace rt {
class Plane : public Object {
   public:
    Plane() : Object(ObjectTypeEnum::Plane) {}

    bool operator==(const Object& rhs) const override;
    Vector normal(const Point& point) const override;
};

std::ostream& operator<<(std::ostream& os, const Plane& plane);

Plane makeDefaultPlane();

class PlaneBuilder {
    public:
    PlaneBuilder& transformation(const Matrix4f& val);
    PlaneBuilder& material(const Material& val);
    std::shared_ptr<Plane> get() const;

    private:
    Plane mPlane{makeDefaultPlane()};
};
}   // namespace rt

#endif /* WORKSPACE_LIB_OBJECTS_INCLUDE_OBJECTS_PLANE */
