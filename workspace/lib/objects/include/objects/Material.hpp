#ifndef WORKSPACE_LIB_TYPES_INCLUDE_TYPES_MATERIAL
#define WORKSPACE_LIB_TYPES_INCLUDE_TYPES_MATERIAL

#include "objects/Light.hpp"
#include "types/Color.hpp"

namespace rt {

struct Material {
    Color color{rt::color::White};

    /// @brief default: 0.1f
    float ambient{0.1f};

    /// @brief default: 0.9f
    float diffuse{0.9f};

    /// @brief default: 0.9f
    float specular{0.9f};

    /// @brief default: 200.0f
    float shininess{200.0f};

    float reflective{0.0f};

    bool operator==(const Material& rhs) const;

    Color lighting(const PointLight& light, const Point& position,
                   const Vector& eyeVec, const Vector& normal,
                   bool inShadow = false) const;
};

Material makeDefaultMaterial();

class MaterialBuilder {
   public:
    MaterialBuilder& ambient(float val);
    MaterialBuilder& diffuse(float val);
    MaterialBuilder& specular(float val);
    MaterialBuilder& shininess(float val);
    MaterialBuilder& color(const Color& val);
    MaterialBuilder& reflective(float val);
    const Material& get() const;

   private:
    Material mMaterial{makeDefaultMaterial()};
};

}   // namespace rt

#endif /* WORKSPACE_LIB_TYPES_INCLUDE_TYPES_MATERIAL */
