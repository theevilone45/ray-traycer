#ifndef WORKSPACE_LIB_OBJECTS_INCLUDE_OBJECTS_SPHERE
#define WORKSPACE_LIB_OBJECTS_INCLUDE_OBJECTS_SPHERE

#include <memory>
#include <ostream>

#include "objects/Material.hpp"
#include "objects/Object.hpp"
#include "types/Matrix.hpp"
#include "types/Point.hpp"
#include "types/Vector.hpp"

namespace rt {
struct Sphere : public Object {
    Sphere() : Object(ObjectTypeEnum::Sphere) {}

    const Point origin{makePoint(0.0f, 0.0f, 0.0f)};
    const float radius{1.0f};
    bool operator==(const Object& rhs) const override;
    Vector normal(const Point& point) const override;
};

std::ostream& operator<<(std::ostream& os, const Sphere& sphere);

/**
 * @brief
 *
 * @return Sphere with radius: 1.0f and origin: Vector(0.0f,0.0f)
 */
Sphere makeDefaultSphere();

class SphereBuilder {
   public:
    SphereBuilder& transformation(const Matrix4f& val);
    SphereBuilder& material(const Material& val);
    std::shared_ptr<Sphere> get() const;

   private:
    Sphere mSphere{rt::makeDefaultSphere()};
};

}   // namespace rt

#endif /* WORKSPACE_LIB_OBJECTS_INCLUDE_OBJECTS_SPHERE */
