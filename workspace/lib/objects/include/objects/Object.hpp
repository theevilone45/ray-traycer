#ifndef WORKSPACE_LIB_OBJECTS_INCLUDE_OBJECTS_OBJECT
#define WORKSPACE_LIB_OBJECTS_INCLUDE_OBJECTS_OBJECT

#include <ostream>

#include "objects/Material.hpp"
#include "types/Matrix.hpp"
#include "types/Point.hpp"
#include "types/Vector.hpp"

namespace rt {

enum class ObjectTypeEnum {
    Sphere,
    Plane
};

std::ostream& operator<<(std::ostream& os, const ObjectTypeEnum& type);

class Object {
   public:
    Object(ObjectTypeEnum type) : type(type) {}

    virtual bool operator==(const Object& rhs) const = 0;
    virtual Vector normal(const Point& point) const = 0;

    Matrix4f transformation{rt::cIdentity4f};
    Material material{makeDefaultMaterial()};

    const ObjectTypeEnum type;
};

}   // namespace rt

#endif /* WORKSPACE_LIB_OBJECTS_INCLUDE_OBJECTS_OBJECT */
