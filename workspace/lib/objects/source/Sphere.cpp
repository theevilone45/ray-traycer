#include "objects/Sphere.hpp"

namespace rt {

bool Sphere::operator==(const Object& rhs) const {
    if (rhs.type != ObjectTypeEnum::Sphere) {
        return false;
    }
    const auto& sphere = static_cast<const Sphere&>(rhs);
    return sphere.origin == origin && sphere.radius == radius &&
           sphere.material == material &&
           sphere.transformation == transformation;
}

Vector Sphere::normal(const Point& point) const {
    const auto transformInv = transformation.inverted();
    const auto objectPoint = transformInv * point;
    const auto objectNormal = objectPoint - rt::makePoint(0.0f, 0.0f, 0.0f);
    auto worldNormal = transformInv.transposed() * objectNormal;
    worldNormal.w = 0.0f;
    return worldNormal.normalized();
}

std::ostream& operator<<(std::ostream& os, const Sphere& sphere) {
    os << "[sphere]\n"
       << "radius: " << sphere.radius << '\n'
       << "origin: " << sphere.origin << '\n';
    return os;
}

Sphere makeDefaultSphere() { return Sphere(); }

SphereBuilder& SphereBuilder::transformation(const Matrix4f& val) {
    mSphere.transformation = val;
    return *this;
}

SphereBuilder& SphereBuilder::material(const Material& val) {
    mSphere.material = val;
    return *this;
}

std::shared_ptr<Sphere> SphereBuilder::get() const {
    return std::make_shared<Sphere>(mSphere);
}

}   // namespace rt
