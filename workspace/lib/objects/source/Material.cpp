#include "objects/Material.hpp"

#include <cmath>

namespace rt {

bool Material::operator==(const Material& rhs) const {
    return color == rhs.color && ambient == rhs.ambient &&
           diffuse == rhs.diffuse && specular == rhs.specular &&
           shininess == rhs.shininess && reflective == rhs.reflective;
}

Color Material::lighting(const PointLight& light, const Point& position,
                         const Vector& eyeVec, const Vector& normal,
                         bool inShadow) const {
    const auto effectiveColor = color.hadamard(light.intensity);
    const auto lightVec = (light.position - position).normalized();
    const auto ambientColor = effectiveColor * ambient;
    const auto lightDotNormal = lightVec.dot(normal);

    rt::Color diffuseColor = color::Black;
    rt::Color specularColor = color::Black;

    if (lightDotNormal >= 0) {
        diffuseColor = effectiveColor * diffuse * lightDotNormal;
        const auto reflectVec = (-lightVec).reflected(normal);
        const auto reflectDotEye = reflectVec.dot(eyeVec);

        if (reflectDotEye > 0) {
            const auto factor = std::pow(reflectDotEye, shininess);
            specularColor = light.intensity * specular * factor;
        }
    }
    if (inShadow) {
        return ambientColor;
    }
    return ambientColor + diffuseColor + specularColor;
}

Material makeDefaultMaterial() { return Material(); }

MaterialBuilder& MaterialBuilder::ambient(float val) {
    mMaterial.ambient = val;
    return *this;
}
MaterialBuilder& MaterialBuilder::diffuse(float val) {
    mMaterial.diffuse = val;
    return *this;
}
MaterialBuilder& MaterialBuilder::specular(float val) {
    mMaterial.specular = val;
    return *this;
}
MaterialBuilder& MaterialBuilder::shininess(float val) {
    mMaterial.shininess = val;
    return *this;
}
MaterialBuilder& MaterialBuilder::color(const Color& val) {
    mMaterial.color = val;
    return *this;
}
MaterialBuilder& MaterialBuilder::reflective(float val) {
    mMaterial.reflective = val;
    return *this;
}

const Material& MaterialBuilder::get() const { return mMaterial; }

}   // namespace rt
