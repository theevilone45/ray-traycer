#include "objects/Object.hpp"

#include <map>
#include <string>

namespace rt {

namespace {
std::map<ObjectTypeEnum, std::string> cObjectTypeStrings = {
    {ObjectTypeEnum::Sphere, "Sphere"}};
}

std::ostream& operator<<(std::ostream& os, const ObjectTypeEnum& type) {
    os << cObjectTypeStrings[type];
    return os;
}
}   // namespace rt
