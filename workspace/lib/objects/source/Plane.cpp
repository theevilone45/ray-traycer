#include "objects/Plane.hpp"

namespace rt {

bool Plane::operator==(const Object& rhs) const {
    if (rhs.type != ObjectTypeEnum::Plane) {
        return false;
    }
    const auto& plane = static_cast<const Plane&>(rhs);
    return plane.material == material && plane.transformation == transformation;
}
Vector Plane::normal([[maybe_unused]] const Point& point) const {
    return makeVector(0.0f,1.0f,0.0f);
}

std::ostream& operator<<(std::ostream& os, const Plane& plane) {
    os << "[plane]\n" << "transformation: " << plane.transformation << '\n';
    return os;
}

Plane makeDefaultPlane() {
    return Plane();
}

PlaneBuilder& PlaneBuilder::transformation(const Matrix4f& val) {
    mPlane.transformation = val;
    return *this;
}

PlaneBuilder& PlaneBuilder::material(const Material& val) {
    mPlane.material = val;
    return *this;
}
std::shared_ptr<Plane> PlaneBuilder::get() const {
    return std::make_shared<Plane>(mPlane);
}

}   // namespace rt
