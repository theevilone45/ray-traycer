#include "objects/Light.hpp"

namespace rt {

PointLight::PointLight(const Color& intensity, const Vector& position)
    : intensity(intensity), position(position) {}

PointLight makePointLight(const Color& intensity, const Vector& position) {
    return PointLight(intensity, position);
}

bool PointLight::operator==(const PointLight& rhs) const{
    return rhs.intensity == intensity && rhs.position == position;
}

PointLight makePointLight()
{
    return PointLight();
}


}   // namespace rt