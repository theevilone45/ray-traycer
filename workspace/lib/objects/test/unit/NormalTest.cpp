#include <gtest/gtest.h>

#include "common/Constants.hpp"
#include "objects/Plane.hpp"
#include "objects/Sphere.hpp"
#include "transformations/Transformations.hpp"
#include "types/Point.hpp"
#include "types/Vector.hpp"

class NormalTest : public ::testing::Test {
   protected:
    NormalTest() = default;
    ~NormalTest() {}

   protected:
    inline const static auto sSphere = rt::makeDefaultSphere();
    inline const static auto sPlane = rt::makeDefaultPlane();
};

TEST_F(NormalTest, XAxisPoint) {
    ASSERT_EQ(sSphere.normal(rt::makePoint(1.0f, 0.0f, 0.0f)),
              rt::makeVector(1.0f, 0.0f, 0.0f));
}

TEST_F(NormalTest, YAxisPoint) {
    ASSERT_EQ(sSphere.normal(rt::makePoint(0.0f, 1.0f, 0.0f)),
              rt::makeVector(0.0f, 1.0f, 0.0f));
}

TEST_F(NormalTest, ZAxisPoint) {
    ASSERT_EQ(sSphere.normal(rt::makePoint(0.0f, 0.0f, 1.0f)),
              rt::makeVector(0.0f, 0.0f, 1.0f));
}

TEST_F(NormalTest, NonAxisPoint) {
    ASSERT_EQ(sSphere.normal(rt::makePoint(rt::cSqrt3 / 3, rt::cSqrt3 / 3,
                                           rt::cSqrt3 / 3)),
              rt::makeVector(rt::cSqrt3 / 3, rt::cSqrt3 / 3, rt::cSqrt3 / 3));
}

TEST_F(NormalTest, NormalIsUnitVector) {
    const auto normal = sSphere.normal(
        rt::makePoint(rt::cSqrt3 / 3, rt::cSqrt3 / 3, rt::cSqrt3 / 3));
    ASSERT_EQ(normal, normal.normalized());
}

TEST_F(NormalTest, TranslatedSphere) {
    auto sphere = rt::makeDefaultSphere();
    sphere.transformation = rt::translation(0.0f, 1.0f, 0.0f);
    const auto normal = sphere.normal(rt::makePoint(0.0f, 1.70711f, -0.70711f));
    ASSERT_EQ(normal, rt::makeVector(0.0f, 0.70711f, -0.70711f));
}

TEST_F(NormalTest, TransformedSphere) {
    auto sphere = rt::makeDefaultSphere();
    sphere.transformation = rt::Transform4f()
                                .zRotate(std::sqrt(5.0f))
                                .scale(1.0f, 0.5f, 1.0f)
                                .get();
    const auto normal = sphere.normal(
        rt::makePoint(0.0f, rt::cSqrt2 / 2.0f, -rt::cSqrt2 / 2.0f));
    ASSERT_EQ(normal, rt::makeVector(0.0f, 0.97014f, -0.24254f));
}

TEST_F(NormalTest, PlaneNormals) {
    ASSERT_EQ(sPlane.normal(rt::makePoint(0.f, 0.f, 0.f)),
              rt::makeVector(0.f, 1.f, 0.f));
    ASSERT_EQ(sPlane.normal(rt::makePoint(10.f, 0.f, -10.f)),
              rt::makeVector(0.f, 1.f, 0.f));
    ASSERT_EQ(sPlane.normal(rt::makePoint(-5.321f, 0.f, 150.4545f)),
              rt::makeVector(0.f, 1.f, 0.f));
}
