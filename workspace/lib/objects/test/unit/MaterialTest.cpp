#include <gtest/gtest.h>

#include "objects/Material.hpp"

TEST(MaterialTest, DefaultMaterial) {
    const auto material = rt::makeDefaultMaterial();
    ASSERT_EQ(material.color, rt::color::White);
    ASSERT_FLOAT_EQ(material.ambient, 0.1f);
    ASSERT_FLOAT_EQ(material.diffuse, 0.9f);
    ASSERT_FLOAT_EQ(material.specular, 0.9f);
    ASSERT_FLOAT_EQ(material.shininess, 200.0f);
    ASSERT_FLOAT_EQ(material.reflective, 0.0f);
}