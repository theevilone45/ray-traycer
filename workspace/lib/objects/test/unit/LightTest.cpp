#include <gtest/gtest.h>

#include "objects/Light.hpp"

TEST(LightTest, PositionAndIntensity) {
    const auto intensity = rt::makeColor(1.0f, 1.0f, 1.0f);
    const auto position = rt::makeVector(0.0f, 0.0f, 0.0f);
    const auto light = rt::makePointLight(intensity, position);

    ASSERT_EQ(light.intensity, intensity);
    ASSERT_EQ(light.position, position);
}