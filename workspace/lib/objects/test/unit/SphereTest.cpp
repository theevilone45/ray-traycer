#include <gtest/gtest.h>

#include "objects/Material.hpp"
#include "objects/Sphere.hpp"

TEST(SphereTest, DefaultMaterial) {
    const auto sphere = rt::makeDefaultSphere();
    ASSERT_EQ(sphere.material, rt::makeDefaultMaterial());
}

TEST(SphereTest, CustomMaterial) {
    auto material = rt::makeDefaultMaterial();
    material.ambient = 1.0f;
    auto sphere = rt::makeDefaultSphere();
    sphere.material = material;

    ASSERT_FLOAT_EQ(sphere.material.ambient, 1.0f);
}