#include <gtest/gtest.h>

#include "common/Constants.hpp"
#include "common/Helpers.hpp"
#include "objects/Light.hpp"
#include "objects/Material.hpp"
#include "types/Point.hpp"
#include "types/Vector.hpp"

class MaterialLightTest : public ::testing::Test {
   protected:
    const rt::Material material = rt::makeDefaultMaterial();
    const rt::Point position = rt::makePoint(0.0f, 0.0f, 0.0f);
};

/**
 * @brief Eye is perpendicular to a wall, light is behind eye, all things are in
 * one line
 */
TEST_F(MaterialLightTest, LightBehindEye) {
    const auto eyeVec = rt::makeVector(0.0f, 0.0f, -1.0f);
    const auto normalVec = rt::makeVector(0.0f, 0.0f, -1.0f);
    const auto light =
        rt::makePointLight(rt::color::White, rt::makePoint(0.0f, 0.0f, -10.0f));
    const auto result = material.lighting(light, position, eyeVec, normalVec);
    ASSERT_EQ(result, rt::makeColor(1.9f, 1.9f, 1.9f));
}

/**
 * @brief Same as above, but eye has 45 deg offset
 */
TEST_F(MaterialLightTest, EyeOffset45) {
    const auto eyeVec =
        rt::makeVector(0.0f, rt::cSqrt2 / 2.0f, rt::cSqrt2 / 2.0f);
    const auto normalVec = rt::makeVector(0.0f, 0.0f, -1.0f);
    const auto light =
        rt::makePointLight(rt::color::White, rt::makePoint(0.0f, 0.0f, -10.0f));
    const auto result = material.lighting(light, position, eyeVec, normalVec);
    ASSERT_EQ(result, rt::color::White);
}

/**
 * @brief Now light has 45 deg, and eye is perpendicular to wall
 */
TEST_F(MaterialLightTest, LightOffset45) {
    const auto eyeVec = rt::makeVector(0.0f, 0.0f, -1.0f);
    const auto normalVec = rt::makeVector(0.0f, 0.0f, -1.0f);
    const auto light = rt::makePointLight(rt::color::White,
                                          rt::makePoint(0.0f, 10.0f, -10.0f));
    const auto result = material.lighting(light, position, eyeVec, normalVec);
    ASSERT_EQ(result, rt::makeColor(0.7364f, 0.7364f, 0.7364f));
}

/**
 * @brief Eye is directly on path of the reflection vector
 */
TEST_F(MaterialLightTest, LightAndEyeOffset45) {
    const auto eyeVec =
        rt::makeVector(0.0f, -rt::cSqrt2 / 2.0f, -rt::cSqrt2 / 2.0f);
    const auto normalVec = rt::makeVector(0.0f, 0.0f, -1.0f);
    const auto light = rt::makePointLight(rt::color::White,
                                          rt::makePoint(0.0f, 10.0f, -10.0f));
    const auto result = material.lighting(light, position, eyeVec, normalVec);
    ASSERT_EQ(result, rt::makeColor(1.63639f, 1.63639f, 1.63639f));
}

/**
 * @brief Light behind a wall
 */
TEST_F(MaterialLightTest, LightBehindWall) {
    const auto eyeVec = rt::makeVector(0.0f, 0.0f, -1.0f);
    const auto normalVec = rt::makeVector(0.0f, 0.0f, -1.0f);
    const auto light =
        rt::makePointLight(rt::color::White, rt::makePoint(0.0f, 0.0f, 10.0f));
    const auto result = material.lighting(light, position, eyeVec, normalVec);
    ASSERT_EQ(result, rt::makeColor(0.1f, 0.1f, 0.1f));
}

TEST_F(MaterialLightTest, SurfaceInShadow) {
    const auto eyeVec = rt::makeVector(0_f, 0_f, -1_f);
    const auto normalVec = rt::makeVector(0_f, 0_f, -1_f);
    const auto light =
        rt::makePointLight(rt::color::White, rt::makePoint(0_f, 0_f, -10_f));
    const auto result =
        material.lighting(light, position, eyeVec, normalVec, true);
    ASSERT_EQ(result, rt::makeColor(0.1f, 0.1f, 0.1f));
}