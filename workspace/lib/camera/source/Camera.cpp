/*
##############################
# LibGen 1.0                 #
# Author: theevilone45       #
# Created: 23.05.2023        #
##############################
 */

#include "camera/Camera.hpp"

#include <cmath>
#include <iostream>

namespace rt {

namespace details {
constexpr const auto cCanvasZCoord = -1.0f;
}   // namespace details

Camera::Camera(uint32_t horizontalSize, uint32_t verticalSize, float fov)
    : horizontalSize(horizontalSize),
      verticalSize(verticalSize),
      fov(fov),
      pixelSize(claculatePixelSize()),
      transform(cIdentity4f) {}

float Camera::claculatePixelSize() {
    const auto halfView = std::tan(fov / 2.0f);
    const auto aspectRatio =
        static_cast<float>(horizontalSize) / static_cast<float>(verticalSize);
    if (aspectRatio >= 1) {
        mHalfWidth = halfView;
        mHalfHeight = halfView / aspectRatio;
    } else {
        mHalfWidth = halfView * aspectRatio;
        mHalfHeight = halfView;
    }
    return (mHalfWidth * 2.0f) / horizontalSize;
}

Ray Camera::rayForPixel(uint32_t x, uint32_t y) const {
    const auto px = static_cast<float>(x);
    const auto py = static_cast<float>(y);
    const auto xOffset = (px + 0.5) * pixelSize;
    const auto yOffset = (py + 0.5) * pixelSize;
    const auto worldX = mHalfWidth - xOffset;
    const auto worldY = mHalfHeight - yOffset;

    const auto transformInv = transform.inverted();
    const auto pixelPosition =
        transformInv * rt::makePoint(worldX, worldY, details::cCanvasZCoord);
    const auto origin = transformInv * rt::makePoint(0.0f, 0.0f, 0.0f);
    const auto direction = (pixelPosition - origin).normalized();

    return rt::makeRay(origin, direction);
}

Canvas Camera::render(const World& world) const {
    auto image = rt::makeCanvas(horizontalSize, verticalSize);
    uint32_t pixelId = 0;
    std::cout << "Pixels to render: " << horizontalSize * verticalSize << "\n";
    for (uint32_t y = 0; y < image.height; y++) {
        for (uint32_t x = 0; x < image.width; x++) {
            const auto ray = rayForPixel(x, y);
            image(x, y) = world.colorAt(ray, cMaxLightBounces);
            std::cout << "\r" << pixelId++ << "    ";
        }
    }
    std::cout << "\nDone.\n";

    return image;
}

CameraBuilder& CameraBuilder::dimentions(uint32_t hSize, uint32_t vSize) {
    mCamera.horizontalSize = hSize;
    mCamera.verticalSize = vSize;
    return *this;
}

CameraBuilder& CameraBuilder::fov(float fov) {
    mCamera.fov = fov;
    return *this;
}

CameraBuilder& CameraBuilder::transform(const rt::Matrix4f& transform) {
    mCamera.transform = transform;
    return *this;
}

const Camera& CameraBuilder::get() {
    mCamera.pixelSize = mCamera.claculatePixelSize();
    return mCamera;
}

Camera makeCamera(uint32_t horizontalSize, uint32_t verticalSize, float fov) {
    return Camera(horizontalSize, verticalSize, fov);
}

}   // namespace rt
