#ifndef WORKSPACE_LIB_CAMERA_INCLUDE_CAMERA_CAMERA
#define WORKSPACE_LIB_CAMERA_INCLUDE_CAMERA_CAMERA

#include "rays/Ray.hpp"
#include "types/Canvas.hpp"
#include "types/Matrix.hpp"
#include "world/World.hpp"

namespace rt {
struct Camera {
    uint32_t horizontalSize = 0;
    uint32_t verticalSize = 0;
    float fov = 0.0f;
    float pixelSize = 0.0f;
    rt::Matrix4f transform = cIdentity4f;

    Camera() = default;
    Camera(uint32_t horizontalSize, uint32_t verticalSize, float fov);

    Ray rayForPixel(uint32_t x, uint32_t y) const;
    Canvas render(const World& world) const;

   private:
    float mHalfWidth;
    float mHalfHeight;

   private:
    float claculatePixelSize();

    friend class CameraBuilder;
};

class CameraBuilder {
   public:
    CameraBuilder& dimentions(uint32_t hSize, uint32_t vSize);
    CameraBuilder& fov(float fov);
    CameraBuilder& transform(const rt::Matrix4f& transform);
    const Camera& get();

   private:
    Camera mCamera;
};

Camera makeCamera(uint32_t horizontalSize, uint32_t verticalSize, float fov);

}   // namespace rt

#endif /* WORKSPACE_LIB_CAMERA_INCLUDE_CAMERA_CAMERA */
