#include <gtest/gtest.h>

#include "camera/Camera.hpp"
#include "common/Constants.hpp"
#include "transformations/View.hpp"
#include "types/Point.hpp"
#include "world/World.hpp"

TEST(RenderingTest, RenderingDefaultWorld) {
    const auto world = rt::makeDefaultWorld();
    const auto from = rt::makePoint(0.0f, 0.0f, -5.0f);
    const auto to = rt::makePoint(0.0f, 0.0f, 0.0f);
    const auto up = rt::makeVector(0.0f, 1.0f, 0.0f);
    const auto camera = rt::CameraBuilder()
                            .dimentions(11, 11)
                            .fov(rt::cPi / 2.0f)
                            .transform(rt::makeView(from, to, up))
                            .get();
    const auto image = camera.render(world);
    ASSERT_EQ(image(5, 5), rt::makeColor(0.38066f, 0.47583f, 0.2855f));
}