#include <gtest/gtest.h>

#include "camera/Camera.hpp"
#include "common/Constants.hpp"
#include "transformations/Transformations.hpp"
#include "types/Matrix.hpp"

TEST(CameraTest, ConstructingCamera) {
    const auto camera = rt::makeCamera(160, 120, rt::cPi / 2);
    ASSERT_EQ(camera.horizontalSize, 160);
    ASSERT_EQ(camera.verticalSize, 120);
    ASSERT_EQ(camera.fov, rt::cPi / 2);
    ASSERT_EQ(camera.transform, rt::cIdentity4f);
}

TEST(CameraTest, HorizontalCanvasPixelSize) {
    const auto camera = rt::makeCamera(200, 125, rt::cPi / 2.0f);
    ASSERT_EQ(camera.pixelSize, 0.01f);
}

TEST(CameraTest, VerticalCanvasPixelSize) {
    const auto camera = rt::makeCamera(125, 200, rt::cPi / 2.0f);
    ASSERT_EQ(camera.pixelSize, 0.01f);
}

TEST(CameraRayTest, CenterOfCanvas) {
    const auto camera = rt::makeCamera(201, 101, rt::cPi / 2.0f);
    const auto ray = camera.rayForPixel(100, 50);
    ASSERT_EQ(ray.origin, rt::makePoint(0.0f, 0.0f, 0.0f));
    ASSERT_EQ(ray.direction, rt::makeVector(0.0f, 0.0f, -1.0f));
}

TEST(CameraRayTest, CornerOfCanvas) {
    const auto camera = rt::makeCamera(201, 101, rt::cPi / 2.0f);
    const auto ray = camera.rayForPixel(0, 0);
    ASSERT_EQ(ray.origin, rt::makePoint(0.0f, 0.0f, 0.0f));
    ASSERT_EQ(ray.direction, rt::makeVector(0.66519f, 0.33259f, -0.66851f));
}

TEST(CameraRayTest, TransformedCamera) {
    auto camera = rt::makeCamera(201, 101, rt::cPi / 2.0f);
    camera.transform = rt::Transform4f()
                           .translate(0.0f, -2.0f, 5.0f)
                           .yRotate(rt::cPi / 4.0f)
                           .get();
    const auto ray = camera.rayForPixel(100, 50);
    ASSERT_EQ(ray.origin, rt::makePoint(0.0f, 2.0f, -5.0f));
    ASSERT_EQ(ray.direction,
              rt::makeVector(rt::cSqrt2 / 2.0f, 0.0f, -rt::cSqrt2 / 2.0f));
}