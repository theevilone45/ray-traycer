
##############################
# LibGen 1.0                 #
# Author: theevilone45       #
# Created: 23.05.2023        #
##############################

set(SOURCES
    main.cpp
    CameraTest.cpp
)

add_executable(CameraUnitTest ${SOURCES})
target_link_libraries(CameraUnitTest PRIVATE gtest_main ${CAMERA_LIB})

gtest_discover_tests(CameraUnitTest)
