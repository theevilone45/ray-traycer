#include <gtest/gtest.h>

#include <ranges>

#include "files/PPMFile.hpp"
#include "types/Canvas.hpp"

TEST(PPMFileTest, ConstructingHeader) {
    const auto file = rt::PPM::convert(rt::makeCanvas(5, 3));
    const std::vector<std::string> expectedHeader = {"P3", "5 3", "255"};

    size_t idx = 0;
    for (const auto& headerLine : expectedHeader) {
        ASSERT_EQ(headerLine, file.at(idx++));
    }
}

TEST(PPMFileTest, ConstructingPixelData) {
    auto canvas = rt::makeCanvas(5, 3);
    const auto color1 = rt::makeColor(1.5, 0, 0);
    const auto color2 = rt::makeColor(0, 0.5, 0);
    const auto color3 = rt::makeColor(-0.5, 0, 1);

    canvas(0, 0) = color1;
    canvas(2, 1) = color2;
    canvas(4, 2) = color3;

    const auto file = rt::PPM::convert(canvas);
    const std::vector<std::string> expectedPixelData = {
        "255 0 0 0 0 0 0 0 0 0 0 0 0 0 0", "0 0 0 0 0 0 0 128 0 0 0 0 0 0 0",
        "0 0 0 0 0 0 0 0 0 0 0 0 0 0 255"};

    size_t idx = 3;
    for (const auto& pixelDataLine : expectedPixelData) {
        ASSERT_EQ(pixelDataLine, file.at(idx++));
    }
}

TEST(PPMFileTest, SplittinLongLines) {
    auto canvas = rt::makeCanvas(10, 2);
    const auto color = rt::makeColor(1, 0.8, 0.6);
    for (auto& pixel : canvas.pixels) {
        pixel = color;
    }
    const std::vector<std::string> expectedPixelData = {
        "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204 "
        "153",
        "255 204 153 255 204 153 255 204 153 255 204 153",
        "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204 "
        "153",
        "255 204 153 255 204 153 255 204 153 255 204 153"};
    const auto file = rt::PPM::convert(canvas);

    size_t idx = 3;
    for (const auto& pixelDataLine : expectedPixelData) {
        ASSERT_EQ(pixelDataLine, file.at(idx++));
    }
}
