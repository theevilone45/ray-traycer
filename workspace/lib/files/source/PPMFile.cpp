#include "files/PPMFile.hpp"

#include <fstream>

namespace rt::PPM {

namespace {
uint8_t convertColorComponent(float component) {
    const float clamped = std::clamp(component, 0.0f, 1.0f);
    return static_cast<uint8_t>(std::ceil(clamped * 255));
}
std::string toString(const Color& color) {
    const auto clampedRed = convertColorComponent(color.red());
    const auto clampedGreen = convertColorComponent(color.green());
    const auto clampedBlue = convertColorComponent(color.blue());
    return std::to_string(clampedRed) + " " + std::to_string(clampedGreen) +
           " " + std::to_string(clampedBlue);
}

constexpr const uint32_t cMaxCharacterInLine = 70;

}   // namespace

FileContent convert(const Canvas& canvas) {
    FileContent result = {
        "P3",
        std::to_string(canvas.width) + " " + std::to_string(canvas.height),
        "255"};

    for (uint32_t h = 0; h < canvas.height; h++) {
        std::string line = "";
        for (uint32_t w = 0; w < canvas.width; w++) {
            if (line.size() >= cMaxCharacterInLine) {
                line.erase(line.size() - 1);
                result.push_back(line);
                line = "";
            }
            const Color color = canvas(w, h);
            line += toString(color) + " ";
        }
        line.erase(line.size() - 1);
        result.push_back(line);
    }

    return result;
}
void save(const FileContent& content, const std::filesystem::path& path) {
    std::ofstream output(path);
    if (!output) {
        throw std::runtime_error("cannot open file: " + path.string());
    }
    for (const auto& line : content) {
        output << line << '\n';
    }
    output.close();
}
void save(const Canvas& content, const std::filesystem::path& path) {
    save(convert(content), path);
}
}   // namespace rt::PPM