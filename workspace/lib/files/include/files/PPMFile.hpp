#ifndef WORKSPACE_LIB_FILES_INCLUDE_FILES_PPMFILE
#define WORKSPACE_LIB_FILES_INCLUDE_FILES_PPMFILE

#include <filesystem>
#include <string>
#include <vector>

#include "types/Canvas.hpp"

namespace rt::PPM {
using FileContent = std::vector<std::string>;
FileContent convert(const Canvas& canvas);
void save(const FileContent& content, const std::filesystem::path& path);
void save(const Canvas& content, const std::filesystem::path& path);
}   // namespace rt::PPM

#endif /* WORKSPACE_LIB_FILES_INCLUDE_FILES_PPMFILE */
