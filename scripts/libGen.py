#!/usr/bin/env python3

import os
import argparse
from datetime import datetime

WORKDIR = os.path.join(os.getcwd(), "workspace", "lib")

CMAKE_FILE = "CMakeLists.txt"

COMMENT_CONTENT = '''
##############################
# LibGen 1.0                 #
# Author: theevilone45       #
# Created: {}        #
##############################
'''.format(datetime.today().strftime("%d.%m.%Y"))
SOURCE_COMMENT_CONTENT = "/* {} */".format(COMMENT_CONTENT)


def help():
    return '''
Hello in libGen 1.0
This will create new CMake lib module with include, source, and tests")
Full location of library will be: {}
    '''.format(os.path.join(WORKDIR, "<lib-name>"))


class CMakeLists:
    def __init__(self, lib_name, source_file_name, dependencies=[]):
        self.lib_name = lib_name
        self.source_file_name = source_file_name
        self.lib_dir = os.path.join(WORKDIR, lib_name)
        self.include_dir = os.path.join(self.lib_dir, "include", self.lib_name)
        self.source_dir = os.path.join(self.lib_dir, "source")
        self.test_dir = os.path.join(self.lib_dir, "test")
        self.include_dir = os.path.join(self.lib_dir, "include")

        # keys are files paths and values are functions generating files content
        self.files_map = {
            self.lib_dir: self.generate_add_sub_cmake(["source", "test"]),
            self.source_dir: self.generate_source_cmake(source_file_name, dependencies),
            self.test_dir: self.generate_add_sub_cmake(["unit"]),
            os.path.join(self.test_dir, "unit"): self.generate_unit_test_cmake(),
        }
        pass

    def generate_non_cmake_files(self):
        with open(os.path.join(self.source_dir, self.source_file_name), "w") as source_file:
            source_file.write(SOURCE_COMMENT_CONTENT)
            pass

        unit_test_main_content = SOURCE_COMMENT_CONTENT
        unit_test_main_content += '''
#include <gtest/gtest.h>

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
        '''
        print(self.test_dir)
        with open(os.path.join(self.test_dir, "unit", "main.cpp"), "w") as main_file:
            main_file.write(unit_test_main_content)
            pass
        pass

    def generate_add_sub_cmake(self, sub_dir_array):
        # cmake containing only add_subdirectory calls
        result = COMMENT_CONTENT
        for item in sub_dir_array:
            result += f"add_subdirectory({item})\n"
        return result

    def generate_source_cmake(self, file_name="main.cpp", dependencies=[]):
        # cmake creating static library definition
        result = COMMENT_CONTENT
        result += '''
set(SOURCES
    {0}
)

add_library(${{{1}_LIB}} SHARED ${{SOURCES}})
target_include_directories(${{{1}_LIB}} PUBLIC ../include)
'''.format(file_name, self.lib_name.upper())

        if dependencies:
            deps_string = ""
            for dep in dependencies:
                deps_string += f"{dep}\n"

            result += '''
set(LIBS
    {0}
)
target_link_libraries(${{{1}_LIB}} PUBLIC ${{LIBS}})
'''.format(deps_string, self.lib_name.upper())

        return result

    def generate_unit_test_cmake(self):
        # code creating test executable
        result = COMMENT_CONTENT
        result += '''
set(SOURCES
    main.cpp
)

add_executable({0}UnitTest ${{SOURCES}})
target_link_libraries({0}UnitTest PRIVATE gtest_main ${{{1}_LIB}})

gtest_discover_tests({0}UnitTest)
'''.format(self.lib_name.capitalize(), self.lib_name.upper())
        return result

    def generate(self):
        for path, content in self.files_map.items():
            current_directory = path
            os.makedirs(path)
            with open(os.path.join(current_directory, CMAKE_FILE), "w") as cmake_file:
                cmake_file.write(content)
                pass
            pass
        self.generate_non_cmake_files()
        os.makedirs(self.include_dir)
        pass
    pass


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=help())
    parser.add_argument("lib_name", help="Library name")
    parser.add_argument("source_file_name")
    parser.add_argument("dependencies", nargs="*",
                        default=[], help="Library dependencies")
    args = parser.parse_args()

    print("Creating library with conf:")
    print(vars(args))

    if not os.path.exists(WORKDIR):
        print("error: given WORKDIR doesn't exists")
        exit(-1)
    if os.path.exists(os.path.join(WORKDIR, args.lib_name)):
        print(f"error: library with name {args.lib_name} already exists")
        exit(-2)

    generator = CMakeLists(
        args.lib_name, args.source_file_name, args.dependencies)
    generator.generate()
    pass
